# Working on func2d-qt6

This is an orientation document for people wanting to work on this project.


## Technology overview

func2d-qt6 is a multi-platform graphical application, developed to
demonstrate how to use OpenGL graphics interoperability with CUDA.
It can, however, be compiled without CUDA Toolkit, and used without GPU.

It uses Qt software development framework for creating its graphical
interface, and for easy handling use of an OpenGL API.

CMake is used as a build system.  It was chosen for being cross-platform,
and to make it easier to integrate Qt and CUDA: both have explicit support
in CMake.


## Setting up for development

func2d-qt6 does not use Qt Forms, nor does it use qmake build system.
Therefore it should be possible to build it without using Qt Creator IDE,
or IDE with specific support for Qt development (perhaps via a plugin).

The build system tries to detect if you have chosen working CUDA
development environment.  You can turn off trying to use CUDA with
`USE_CUDA_IF_POSSIBLE` CMake build setting (which defaults to true).

For compiling CUDA code beside CUDA Toolkit you also need to use
the correct host compiler for the operating system.  The following compilers
must be used with CUDA Toolkit:
- [for Microsoft Windows][cuda-win] you need supported version of Microsoft Visual Studio
- [for Linux][cuda-linux] you need supported version of a gcc compiler and toolchain
- CUDA Toolkit 11.x no longer supports macOS (MacOS X);<br />
  [CUDA 10.2 for MacOS X][cuda-macos] required Clang compiler and Xcode toolchain

[cuda-win]: https://docs.nvidia.com/cuda/cuda-installation-guide-microsoft-windows/index.html "CUDA Installation Guide for Microsoft Windows"
[cuda-linux]: https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html "NVIDIA CUDA Installation Guide for Linux"
[cuda-macos]: https://docs.nvidia.com/cuda/archive/10.2/cuda-installation-guide-mac-os-x/index.html "NVIDIA CUDA 10.2 Installation Guide for Mac OS X"


### Developing on Microsoft Windows

This section describes how to configure specific editors and IDEs which
were _tested_ to successfully build and run `func2d-qt6`.

#### Running built `func2-qt6.exe` from IDE

When using Qt Creator, it sets up environment variables before launching
the application.  If you try to launch the executable directly, which
includes trying to launch it by any other IDE, it would fail because it
cannot find Qt libraries, for example _"The code execution cannot proceed
because Qt6Core.dll was not found"_.

One possible solution is to use [Qt deployment tool for MS Windows][qt-win-deploy],
`windeployqt.exe`, running it with path to the generated `func2d-qt6.exe`
executable as a parameter.  This tool would then copy all required DLL files
to the directory with the application binary executable.

Note that the path to the executable depends on the configured build dir,
which might be different for different IDEs.

[qt-win-deploy]: https://doc.qt.io/qt-6/windows-deployment.html "Qt for Windows - Deployment | Qt Documentation"

#### Microsoft Visual Studio

<u>Tested with Microsoft Visual Studio Studio Community 2019 version 16.11.20.</u>

Recommended plugins:
- [NVIDIA Nsight Developer Tools Integration for Visual Studio][nsight-vs]
  by NVIDIA DevTools Team (see [its documentation][nsight-vs-docs]),
- [Qt Visual Studio Tools][qtvstools] by The Qt Company,
- [GLSL language integration][DanielScherzer.GLSL] by Daniel Scherzer
  _(optional)_.

[nsight-vs]: https://developer.nvidia.com/nsight-tools-visual-studio-integration "NVIDIA Nsight Integration | NVIDIA Developer"
[nsight-vs-docs]: https://docs.nvidia.com/nsight-vs-integration/index.html "NVIDIA Nsight Integration Documentation"
[qtvstools]: https://doc.qt.io/qtvstools/index.html "Qt VS Tools Manual"
[DanielScherzer.GLSL]: https://marketplace.visualstudio.com/items?itemName=DanielScherzer.GLSL "GLSL language integration - Visual Studio Marketplace"

When compiling func2d-qt6 with MS Visual Studio, you might need to tell it
where to find Qt.  One possible solution is to [customize CMake build settings][cmake-vs]
locally, using `CMakeSettings.json` file ([or `CMakePresets.json`][cmake-presets-vs]).

To open the CMake settings editor, select the **Configuration** drop-down
in the main toolbar and choose **Manage Configurations**.  Alternatively,
you can just open `CMakeSettings.json` file, if it exists, in Visual Studio.

If during the build process, cmake fails with the error message that it
cannot find the FindQt module (`Qt6Config.cmake` or `qt6-config.cmake`),
you might need to set **`QT_DIR`** build variable (and possibly also `Qt6_DIR`;
note that this configuration would appear only after setting `QT_DIR` and
re-running CMake) to the directory containing `Qt6Config.cmake`, for example
to `C:/Qt/6.4.0/msvc2019_64/lib/cmake/Qt6`.

Possible alternatives include setting `CMAKE_PREFIX_PATH`, or using the
`qt-cmake.bat` script from Qt (e.g `C:\Qt\6.4.0\msvc2019_64\bin\qt-cmake.bat`)
as CMake executable.

If you have an older NVIDIA GPU, with _compute capability_ (which identifies
the features supported by the GPU hardware) that is older than the
[default compute architecture][nvcc-gpu-name] that `nvcc` compiler assumes,
you might need to set **`CMAKE_CUDA_ARCHITECTURES`** build variable.
You can find compute capability of your NVIDIA graphics card on
[GPU Compute Capability list][cuda-gpus], or you can search the
[GPU Specs Database][gpu-specs].  For example, CUDA 11.0 uses `sm_52`
(compute capability 5.2) as the defaut; if you have a graphics card with
compute capability 5.0, you can set `CMAKE_CUDA_ARCHITECTURES` to
a single-element list: "`50`".

Below there is an example of `CMakeSettings.json` file:
```json
{
  "configurations": [
    {
      "name": "x64-Debug (default)",
      "generator": "Ninja",
      "configurationType": "Debug",
      "inheritEnvironments": [ "msvc_x64_x64" ],
      "buildRoot": "${projectDir}\\out\\build\\${name}",
      "installRoot": "${projectDir}\\out\\install\\${name}",
      "cmakeCommandArgs": "",
      "buildCommandArgs": "",
      "ctestCommandArgs": "",
      "variables": [
        {
          "name": "QT_DIR",
          "value": "C:/Qt/6.4.0/msvc2019_64",
          "type": "PATH"
        },
        {
          "name": "Qt6_DIR",
          "value": "C:/Qt/6.4.0/msvc2019_64/lib/cmake/Qt6",
          "type": "PATH"
        },
        {
          "name": "CMAKE_CUDA_ARCHITECTURES",
          "value": "50",
          "type": "STRING"
        }
      ]
    }
  ]
}

```

[cmake-vs]: https://learn.microsoft.com/en-us/cpp/build/customize-cmake-settings?view=msvc-170 "Customize CMake build settings in Visual Studio | Microsoft Learn"
[cmake-presets-vs]: https://learn.microsoft.com/en-us/cpp/build/cmake-presets-vs?view=msvc-170 "Configure and build with CMake Presets | Microsoft Learn"
[nvcc-gpu-name]: https://docs.nvidia.com/cuda/cuda-compiler-driver-nvcc/index.html#ptxas-options-gpu-name "NVCC Command Options: --gpu-name / -arch :: CUDA Toolkit Documentation"
[cuda-gpus]: https://developer.nvidia.com/cuda-gpus "CUDA GPUs - Compute Capability | NVIDIA Developer"
[gpu-specs]: https://www.techpowerup.com/gpu-specs/ "GPU Database | TechPowerUp"

<u>**Troubleshooting:**</u> Sometimes there is an error when running CMake,
in the "Check for working CUDA compiler" step, namely LNK1109 fatal error
that the linker cannot open file "kernel32.lib" (during the build of a test
program).  What fixed it for me was switching to a CUDA Sample project
(or possibly to any project using `*.vcxproj`, instead of using CMake),
building it, and switching back to func2d-qt6.

#### Qt Creator

On Microsoft Windows compiling CUDA requires MSVC 64-bit.  Therefore you
need to have MSVC support, adding it with Qt Maintenance Tool if necessary.
If CMakeLists.txt didn't prevent it, compiling with MinGW would fail at
the running cmake stage, during 'Compiling the CUDA compiler identification
source file "CMakeCUDACompilerId.cu"', with:

> nvcc fatal : Cannot find compiler 'cl.exe' in PATH

If you have an older NVIDIA GPU, with _compute capability_ (which identifies
the features supported by the GPU hardware) that is older than the
[default compute architecture][nvcc-gpu-name] that `nvcc` compiler assumes,
you might need to set or change **`CMAKE_CUDA_ARCHITECTURES`** build variable.
When the plot is empty when running calculations on the GPU, this might be
the case.

To do this in Qt Creator, select **Projects** in the toolbar on the left
hand side of the screen, find **Build & Run**, and select **Build** for
the correct kit (e.g. _Desktop Qt 6.4.0 MSVC2019 64bit_), and find
**Build Settings** there: see [Specifying Build Settings][qtcreator-build]
in the Qt Creator Manual.  (If you want to specify initial build configuration
for all project using specific build kit, see [CMake Build Configuration][qtcreator-build-init])

[qtcreator-build]: https://doc.qt.io/qtcreator/creator-build-settings.html "Specifying Build Settings | Qt Creator Manual"
[qtcreator-build-init]: https://doc.qt.io/qtcreator/creator-build-settings-cmake.html "CMake Build Configuration | Qt Creator Manual"

<u>**Troubleshooting:**</u> If there is an error when running cmake, namely
that `CMAKE_CXX_COMPILER` is not a full path to an existing compiler tool,
this might mean that Visual Studio was updated, and the path is no longer
correct.  This may require going to the Qt Creator configuration
(menu Edit -> Preferences), go to **Kits**, and possibly run "Re-detect" action
in the **Compilers** tab, but what is in the following paragraph might be
enough.

You may also need to clean CMake configuration, rescan project, and run CMake
(all from the **Build** menu).

#### CLion IDE by JetBrains

Recommended plugins:
* Either [GLSL Support][6993-glsl-support] (simple, doesn't require restart),
  or [GLSL][18470-glsl] (more advanced)

[6993-glsl-support]: https://plugins.jetbrains.com/plugin/6993-glsl-support "GLSL Support by Foundation - IntelliJ IDEs Plugin | JetBrains Marketplace"
[18470-glsl]: https://plugins.jetbrains.com/plugin/18470-glsl "GLSL by Walt Grace - IntelliJ IDEs Plugin | JetBrains Marketplace"

_The Markdown plugin is bundled with CLion, and the support for CMake and for
CUDA C++ is built in into the IDE._

To adjust CMake configuration settings, click on the gear icon near top right
corner of the IDE, select **Settings** menu item, go to **Build, Execution,
Deployment** section, and then to **CMake** subsection.  There you can find
an option to set **cache variables**.

You might want to set `CMAKE_PREFIX_PATH` variable to the path to Qt with MSVC,
for example "C:/Qt/6.4.0/msvc2019_64" (you need to adjust this path to
your configuration).  You might also want to set `CMAKE_CUDA_ARCHITECTURES`
if you have an older GPU.

CLion configuration, including CMake cache variables configuration,
is stored in '`.idea/workspace.xml`' untracked file.

<u>**Troubleshooting**</u> If there is an error during linking, that is the
last part of the build process, where `nvcc.exe` fails to link with
mysterious error, what might help is to switch **Generator** from _"Let CMake decide"_
to _"Use default (ninja)"_.


### Developing on Linux

This section describes how to configure specific editors and IDEs which
were _tested_ to successfully build and run `func2d-qt6`, and how to
configure and build this aplication from the command line.

#### Building application from the command line

To configure application, run `cmake .` in the main directory
of the application (with 'CMakeLists.txt' file).  If it cannot
find Qt, you can use `qt-cmake` script from Qt, for example
`/usr/lib/qt6/bin/qt-cmake .`.

To pass configuration variables to `cmake`, use `-D` parameter,
for example `/usr/lib/qt6/bin/qt-cmake . -DCUDA_ARCHITECTURES="61"`.

The default build system on Linux is make, so it should be enough
to just run `make` in the top directory of the project.  You can
use `cmake --build .` instead.

#### Running func2d-qt6 on builtin + discrete GPU systems

If the computer include both built-in low power GPU for simple
tasks, in addition to NVIDIA GPU, you might need to force to
use NVIDIA GPU for specific application.

One solution is to set GPU to use with environment variables:
```
$ __NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia ./func2d-qt6
```
