#ifndef GRID2D_H
#define GRID2D_H

#include <cstdlib>
#include <vector>
#include <ostream>

#include "common.h"


struct Point2D
{
    float x;
    float y;
};

// This class is created to be trivial class type, that is POD (Plain Old Data) type
struct Grid2D
{
    Grid2D() = default;
    Grid2D(Point2D origin,
           float scale_x, float scale_y,
           size_t Nx, size_t Ny);
    Grid2D(float x_min, float x_max,
           float y_min, float y_max,
           size_t Nx, size_t Ny);

    // getters and getter-like methods
    __host__ __device__ Point2D origin() const { return m_origin; }
    __host__ __device__ float dx() const { return m_dx; }
    __host__ __device__ float dy() const { return m_dy; }
    __host__ __device__ float x_min() const { return m_origin.x; }
    __host__ __device__ float y_min() const { return m_origin.y; }
    __host__ __device__ float x_max() const { return m_origin.x + (m_Nx - 1) * m_dx; }
    __host__ __device__ float y_max() const { return m_origin.y + (m_Ny - 1) * m_dy; }
    __host__ __device__ size_t Nx() const { return m_Nx; }
    __host__ __device__ size_t Ny() const { return m_Ny; }

    // setters and setter-like methods
    // TODO: fluent interface???
    __host__ __device__ void moveTo(Point2D new_origin) { m_origin = new_origin; }
    __host__ __device__ void moveRel(Point2D delta) { m_origin.x += delta.x; m_origin.y += delta.y; }
    __host__ __device__ void setOrigin(Point2D origin) { moveTo(origin); }
    __host__ __device__ void setDx(float dx) { m_dx = dx; }
    __host__ __device__ void setDy(float dy) { m_dy = dy; }
    // ... (when needed) ...

    // points on grid
    __host__ __device__
    Point2D operator()(int i, int j) const {
        return Point2D{m_origin.x + i*m_dx,
                       m_origin.y + j*m_dy};
    }
    __host__ __device__
    Point2D operator()(size_t i, size_t j) const {
        return Point2D{m_origin.x + i*m_dx,
                       m_origin.y + j*m_dy};
    }
    __host__ __device__ float xi(int ix) const { return m_origin.x + ix*m_dx; }
    __host__ __device__ float yi(int iy) const { return m_origin.y + iy*m_dy; }
    __host__ __device__ float xi(int ix, int) const { return m_origin.x + ix*m_dx; }
    __host__ __device__ float yi(int, int iy) const { return m_origin.y + iy*m_dy; }
    __host__ __device__ float xi(size_t ix, size_t) const { return m_origin.x + ix*m_dx; }
    __host__ __device__ float yi(size_t, size_t iy) const { return m_origin.y + iy*m_dy; }

    // flattened index - decide whether 'x' are in row or col
    // use C-style row-first matrix layout
    __host__ __device__ size_t idx(size_t ix, size_t iy) const { return m_Nx*ix + iy; }
    __host__ __device__ size_t n_points() const { return m_Nx*m_Ny; }

    // TODO: iterators
    //
    // https://stackoverflow.com/questions/8054273/how-to-implement-an-stl-style-iterator-and-avoid-common-pitfalls
    // https://www.fluentcpp.com/2018/05/08/std-iterator-deprecated/

    // fill (x, y) values, constant for grid
    void fill_xy(std::vector<Point2D> &xy) const;
    void fill_xy(std::vector<float> &x,
                 std::vector<float> &y) const;
    // unsafe version of filling (x, y) values of grid
    void fill_xy(Point2D xy[]) const;
    void fill_xy(float x[], float y[]) const;

    // data output / pretty printing
    friend std::ostream& operator<<(std::ostream& out, const Grid2D& grid);

private:
    // main data, defining infinite grid of points
    Point2D m_origin{0.0f, 0.0f};
    float m_dx = 1.0f;
    float m_dy = 1.0f;

    // supplementary data, defining finite grid
    size_t m_Nx = 1;
    size_t m_Ny = 1;
};

#endif // GRID2D_H
