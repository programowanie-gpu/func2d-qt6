#ifndef TEXTUREDRECTGEOMETRYENGINE_H
#define TEXTUREDRECTGEOMETRYENGINE_H

#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>

class TexturedRectGeometryEngine : protected QOpenGLFunctions
{
public:
    TexturedRectGeometryEngine();
    virtual ~TexturedRectGeometryEngine();

    void drawGeometry(QOpenGLShaderProgram *texturingShaderProgram);

private:
    void initGeometry();

    QOpenGLBuffer arrayBuf{QOpenGLBuffer::VertexBuffer}; //< VBO for vertex data
    QOpenGLBuffer indexBuf{QOpenGLBuffer::IndexBuffer};  //< VBO for indices to vertices
    int numVerticesData; //< vertex data array size, number of vertices (with repetitions)
    int numDrawElements; //< element indices array size, to be passed as `count` to glDrawElements()
};

#endif // TEXTUREDRECTGEOMETRYENGINE_H
