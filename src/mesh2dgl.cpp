// this header file contains only preprocessor macros and defines
#include "common.h" // IWYU pragma: keep

#include "mesh2dgl.h"

#ifndef NDEBUG
// debugging
#include <QtGlobal>
#endif


void Mesh2DGL::initializeGL()
{
#ifndef NDEBUG
    qDebug("[%s] start for %s", __PRETTY_FUNCTION__, typeid(*this).name());
#endif

    initializeOpenGLFunctions();

    // create buffer object if needed
    if (!m_indexBuffer.isCreated()) {
#ifndef NDEBUG
        qDebug("  creating IBO");
#endif
        m_indexBuffer.create();
        m_indexBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
    }

    // allocate and copy index data
    m_indexBuffer.bind();
    m_indexBuffer.allocate(m_indexData.data(),
                           m_indexData.size()*sizeof(GLuint));
    m_indexBuffer.release();

#ifndef NDEBUG
    qDebug("  created IBO [%d] with %zd elements",
           m_indexBuffer.bufferId(), m_indexData.size());
    qDebug("  mode = 0x%04d, count = %d, index_type = 0x%04x",
           draw_mode(), count(), index_type());

    if (count() >= 7) {
        if (draw_mode() == GL_TRIANGLES) {
            qDebug("  data = {%u, %u, %u; %u, %u, %u;..., %u}",
                   m_indexData[0], m_indexData[1], m_indexData[2],
                   m_indexData[3], m_indexData[4], m_indexData[5],
                   m_indexData.back());
        } else if (draw_mode() == GL_LINES) {
            qDebug("  data = {%u, %u; %u, %u; %u, %u;..., %u}",
                   m_indexData[0], m_indexData[1], m_indexData[2],
                   m_indexData[3], m_indexData[4], m_indexData[5],
                   m_indexData.back());
        } else {
            qDebug("  data = {%u, %u, %u, %u, %u, %u,..., %u}",
                   m_indexData[0], m_indexData[1], m_indexData[2],
                   m_indexData[3], m_indexData[4], m_indexData[5],
                   m_indexData.back());
        }
    }

    qDebug("[%s] end", __PRETTY_FUNCTION__);
#endif // !defined(NDEBUG)
}

void Mesh2DGL::drawElements()
{
    if (!m_indexBuffer.bind()) {
        qWarning("Could not bind index buffer for Mesh2DGL!");
        glDrawElements(draw_mode(), count(), index_type(), m_indexData.data());

        return;
    }

    glDrawElements(draw_mode(), count(), index_type(), nullptr);
    m_indexBuffer.release();
}

// here only to provide out-of-line definition of virtual function
GLenum Surface::Mesh::index_type() const { return GL_UNSIGNED_INT; }
GLenum Surface::Grid::index_type() const { return GL_UNSIGNED_INT; }


Surface::MeshTriangles::MeshTriangles(GLuint Nx, GLuint Ny)
{
#ifndef NDEBUG
    qDebug("[%s] start", __PRETTY_FUNCTION__);
#endif

    /*
     *  (x  ,y  )        (x+1,y  )    (x  ,y  )        (x+1,y  )
     *            0----1                        4
     *             \   |   	   	   	   	   	   	|\
     *              \  |						| \
     *               \ |						|  \
     *                \|						|	\
     *                 2                        6----5
     *  (x  ,y+1)        (x+1,y+1)    (x  ,y+1)        (x+1,y+1)
     *
     *  provides clockwise (GL_CW) orientation
     *  (the default front face is GL_CCW)
     */
    m_indexData.reserve(2*3*(Nx-1)*(Ny-1));

    for (GLuint iy = 0; iy+1 < Ny; iy++) {
        for (GLuint ix = 0; ix+1 < Nx; ix++) {
            m_indexData.push_back( iy      * Nx + ix);
            m_indexData.push_back( iy      * Nx + ix + 1);
            m_indexData.push_back((iy + 1) * Nx + ix + 1);

            m_indexData.push_back( iy      * Nx + ix);
            m_indexData.push_back((iy + 1) * Nx + ix + 1);
            m_indexData.push_back((iy + 1) * Nx + ix);
        } /* end for ix */
    } /* end for iy */

#ifndef NDEBUG
    qDebug("  created IBO data with %zd elements (2*3*(%d-1)*(%d-1))",
           m_indexData.size(), Nx, Ny);
    qDebug("[%s] end", __PRETTY_FUNCTION__);
#endif
}

GLenum Surface::MeshTriangles::draw_mode() const
{
    return GL_TRIANGLES;
}


// https://en.wikibooks.org/wiki/OpenGL_Programming/Scientific_OpenGL_Tutorial_04
// https://en.wikibooks.org/wiki/OpenGL_Programming/Scientific_OpenGL_Tutorial_05
// https://gitlab.com/wikibooks-opengl/modern-tutorials/blob/master/graph05/graph.cpp
Surface::GridLines::GridLines(GLuint Nx, GLuint Ny)
{
#ifndef NDEBUG
    qDebug("[%s] start", __PRETTY_FUNCTION__);
#endif

    /*
     *  (x  ,y  )  (x+1,y  )  (x+2,y  )   ...
     *      0----------1          4-------...
     *                                    ...
     *                 2----------3       ...
     *
     * and similarly for vertical lines
     */
    m_indexData.reserve(2*(Nx-1)*(Ny+0)+
                        2*(Nx+0)*(Ny-1));

    // horizontal lines, along Ox axis
    for (GLuint iy = 0; iy < Ny; iy++) {
        for (GLuint ix = 0; ix+1 < Nx; ix++) {
            m_indexData.push_back( iy      * Nx + ix);
            m_indexData.push_back( iy      * Nx + ix + 1);
        } /* end for ix */
    } /* end for iy */

    // vertical lines, along Oy axis
    for (GLuint ix = 0; ix < Nx; ix++) {
        for (GLuint iy = 0; iy+1 < Ny; iy++) {
            m_indexData.push_back( iy      * Nx + ix);
            m_indexData.push_back((iy + 1) * Nx + ix);
        } /* end for iy */
    } /* end for ix */

#ifndef NDEBUG
    qDebug("  created IBO data with %zd elements (%d x %d grid)",
           m_indexData.size(), Nx, Ny);
    qDebug("[%s] end", __PRETTY_FUNCTION__);
#endif
}

GLenum Surface::GridLines::draw_mode() const
{
    return GL_LINES;
}
