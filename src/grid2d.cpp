#include "grid2d.h"

// turn off debugging for now, when Grid2D is created for each paint
//#define NDEBUG

#ifndef NDEBUG
// debug
#include <iostream>
#endif


__host__ __device__
Grid2D::Grid2D(Point2D origin,
               float scale_x, float scale_y,
               size_t Nx, size_t Ny)
    : m_origin{origin},
      m_dx{scale_x},
      m_dy{scale_y},
      m_Nx{Nx},
      m_Ny{Ny}
{
#if !defined(__CUDA_ARCH__) && !defined(NDEBUG) // use only in __host__ code, and when debugging
    std::cerr << __PRETTY_FUNCTION__ << std::endl;
#endif
}

__host__ __device__
Grid2D::Grid2D(float x_min, float x_max,
               float y_min, float y_max,
               size_t Nx, size_t Ny)
    : m_origin{x_min, y_min},
      m_Nx{Nx},
      m_Ny{Ny},
      m_dx{0.0f},
      m_dy{0.0f}
{
    // we want x_max == x_min + (Nx-1)*dx, etc.
    // if Nx <= 1, then dx doesn't matter, can be 1.0f, can be 0.0f
    if (Nx > 1)
        m_dx = (x_max - x_min) / (Nx - 1);
    if (Ny > 1)
        m_dy = (y_max - y_min) / (Ny - 1);

#if !defined(__CUDA_ARCH__) && !defined(NDEBUG) // use only in __host__ code, and when debugging
    std::cerr << __PRETTY_FUNCTION__ << std::endl;
    std::cerr << "  size = " << m_Nx << " x " << m_Ny << std::endl;
    std::cerr << "  range: ["
              << x_min << "," << x_max << "] x ["
              << y_min << "," << y_max << "]" << std::endl;
    std::cerr << "  center: ("
              << 0.5f*(x_max + x_min) << ","
              << 0.5f*(y_max + y_min) << ")" << std::endl;
    std::cerr << "  origin = (" << m_origin.x << "," << m_origin.y << ")" << std::endl;
    std::cerr << "  delta = " << m_dx << " x " << m_dy << std::endl;
#endif
}

void Grid2D::fill_xy(std::vector<Point2D> &xy) const
{
    xy.resize(n_points());
    fill_xy(xy.data());
}

void Grid2D::fill_xy(std::vector<float> &x,
                     std::vector<float> &y) const
{
    x.resize(n_points());
    y.resize(n_points());
    fill_xy(x.data(), y.data());
}

void Grid2D::fill_xy(Point2D xy[]) const
{
    for (size_t i = 0; i < m_Nx; i++) {
        for (size_t j = 0; j < m_Ny; j++) {
            xy[idx(i, j)] = operator()(i, j);
        }
    }
}

void Grid2D::fill_xy(float x[], float y[]) const
{
    for (size_t i = 0; i < m_Nx; i++) {
        for (size_t j = 0; j < m_Ny; j++) {
            x[idx(i, j)] = xi(i, j);
            y[idx(i, j)] = yi(i, j);
        }
    }
}


std::ostream &operator<<(std::ostream &out, const Grid2D &grid)
{
    out << "Grid2D(["
        << grid.x_min() << "," << grid.x_max() << "] x ["
        << grid.y_min() << "," << grid.y_max() << "], ("
        << grid.Nx() << "," << grid.Ny() << "))";

    return out;
}
