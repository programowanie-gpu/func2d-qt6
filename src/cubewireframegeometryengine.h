#ifndef CUBEWIREFRAMEGEOMETRYENGINE_H
#define CUBEWIREFRAMEGEOMETRYENGINE_H

#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>

class CubeWireframeGeometryEngine : protected QOpenGLFunctions
{
public:
    CubeWireframeGeometryEngine();
    virtual ~CubeWireframeGeometryEngine();

    void drawGeometry(QOpenGLShaderProgram *coloringShaderProgram);

private:
    void initCubeWireframeGeometry();

    QOpenGLBuffer arrayBuf{QOpenGLBuffer::VertexBuffer};
    QOpenGLBuffer indexBuf{QOpenGLBuffer::IndexBuffer};
    int numVerticesData; // vertex data array size
    int numDrawElements; // element indexes array size
};

#endif // CUBEWIREFRAMEGEOMETRYENGINE_H
