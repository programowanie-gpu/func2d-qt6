#ifndef PLOT2D_GPU_H
#define PLOT2D_GPU_H

#include "grid2d.h"


void plot_func2d_gpu(unsigned char pixmap[], Grid2D grid);
void plot_func2d_gpu(float pixmap[], Grid2D grid);

#endif // PLOT2D_GPU_H
