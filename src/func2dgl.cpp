#include <cmath>

// this header file contains only preprocessor macros and defines
#include "common.h" // IWYU pragma: keep

#include "func2dgl.h"
#include "grid2d.h"
#include "plot2d_cpu.h"


Func2DGL::Func2DGL(QOpenGLBuffer& pixmapBuf,
                   float x0, float y0, float x1, float y1,
                   int Nx, int Ny)
    : Func2DBase(pixmapBuf, x0, y0, x1, y1, Nx, Ny)
{
#ifndef NDEBUG
    qDebug("[%s] allocating %zd bytes for %dx%d bitmap (of float 'z' values)",
           __PRETTY_FUNCTION__, Nx*Ny*sizeof(float), Nx, Ny);
#endif

    // allocate data for values
    m_pixmap = new float[Nx * Ny];
}

Func2DGL::~Func2DGL()
{
    delete[] m_pixmap;

#ifndef NDEBUG
    qDebug("[%s] freed memory", __PRETTY_FUNCTION__);
#endif
}

void Func2DGL::evaluate()
{
#ifndef NDEBUG
    static bool first_run = true;
    if (first_run) {
        qDebug("[%s] plot x=[%.2f:%.2f], y=[%.2f:%.2f] on [ptr=%p/VBO=%d]", __PRETTY_FUNCTION__,
               m_grid.x_min(), m_grid.x_max(), m_grid.y_min(), m_grid.y_max(),
               m_pixmap, m_pixmapBuf.bufferId());
        first_run = false;
    }
#endif

    plot_func2d_cpu(m_pixmap, m_grid);

    m_pixmapBuf.bind();
    m_pixmapBuf.write(0, m_pixmap, m_grid.n_points() * sizeof(m_pixmap[0]));
    m_pixmapBuf.release();
}
