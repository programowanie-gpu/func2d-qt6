#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QCheckBox>
#include <QGroupBox>
#include <QKeyEvent>

#include "mainwidget.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    // QWidget interface
    void keyPressEvent(QKeyEvent *event) override;

private:
    void createDockWindows();
    QGroupBox *createDrawingGroup();
    QGroupBox *createViewingGroup();
    QGroupBox *createModelRotationGroup();

    MainWidget *mainWidget;
    QCheckBox *drawSurfaceCheckBox;
    QCheckBox *drawLinesCheckBox;
    QCheckBox *drawPointsCheckBox;

public slots:
    void showMessageInStatusbar(const QString &message);
    void updatePlot2dCheckboxes(const SurfacePlotGL *plot2d);
};
#endif // MAINWINDOW_H
