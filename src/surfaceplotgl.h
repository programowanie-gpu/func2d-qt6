#ifndef SURFACEPLOTGL_H
#define SURFACEPLOTGL_H

#include <memory>

#include <QMatrix4x4>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>

#include "func2dbase.h"
#include "mesh2dgl.h"


class SurfacePlotGL : protected QOpenGLFunctions
{
public:
    explicit SurfacePlotGL(Func2DBase *plottedFunction,
                           std::unique_ptr<Surface::Mesh> surfaceMesh = nullptr,
                           std::unique_ptr<Surface::Grid> surfaceGrid = nullptr)
        : m_plottedFunction{plottedFunction},
          m_surfaceMesh{std::move(surfaceMesh)},
          m_surfaceGrid{std::move(surfaceGrid)}
    {
#ifndef NDEBUG
        qDebug("[%s] function ptr = %p", __PRETTY_FUNCTION__, m_plottedFunction);
#endif
        if (!m_surfaceMesh)
            m_surfaceMesh = std::make_unique<Surface::MeshTriangles>(m_plottedFunction->grid());
        if (!m_surfaceGrid)
            m_surfaceGrid = std::make_unique<Surface::GridLines>(m_plottedFunction->grid());
        initializeGL();
    };
    ~SurfacePlotGL() { cleanup(); }

    void draw(QMatrix4x4 mvpMatrix);

    // turn on and off drawing surface, wireframe, points
    // MAYBE: make it private, use getters and setters
    bool checkDrawSurface = true;
    bool checkDrawLines   = true;
    bool checkDrawPoints  = false;
    // turn on and off polygon offset (see glPolygonOffset)
    bool polygonOffset = true;

private:
    // function to be plotted, together with grid
    Func2DBase *m_plottedFunction;

    std::unique_ptr<Surface::Mesh> m_surfaceMesh; ///< geometry for drawing surface plot
    std::unique_ptr<Surface::Grid> m_surfaceGrid; ///< geometry for drawing grid lines

    // OpenGL drawing
    QOpenGLShaderProgram m_plotShaderProgram; // for z = f(x,y), i.e. surface, lines, points
    QOpenGLBuffer m_vertexXY{QOpenGLBuffer::VertexBuffer};

    // named after method names in QOpenGLWidget
    void initializeGL();
    void cleanup();
};

#endif // SURFACEPLOTGL_H
