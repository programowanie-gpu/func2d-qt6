#include <map>

#include "openglutils.h"


const char* describeQOpenGLBufferType(QOpenGLBuffer::Type type)
{
    const std::map<QOpenGLBuffer::Type, const char*> name_map{
            {QOpenGLBuffer::VertexBuffer, "QOpenGLBuffer::VertexBuffer"},
            {QOpenGLBuffer::IndexBuffer, "QOpenGLBuffer::IndexBuffer"},
            {QOpenGLBuffer::PixelPackBuffer, "QOpenGLBuffer::PixelPackBuffer"},
            {QOpenGLBuffer::PixelUnpackBuffer, "QOpenGLBuffer::PixelUnpackBuffer"}
    };

    auto   it  = name_map.find(type);
    return it == name_map.end() ? "Unknown QOpenGLBuffer.type()" : it->second;
}
