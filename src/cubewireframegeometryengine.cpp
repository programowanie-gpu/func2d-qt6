// this 'common.h' header file contains only preprocessor macros and defines
#include "common.h" // IWYU pragma: keep
#include "cubewireframegeometryengine.h"

CubeWireframeGeometryEngine::CubeWireframeGeometryEngine()
    : numVerticesData(8), // 8 vertices in a cube
      numDrawElements(24) // 12 edges * 2 vertices per edge
{
#ifndef NDEBUG
    qDebug("[%s]", __PRETTY_FUNCTION__);
#endif
    initializeOpenGLFunctions();

    // Generate 2 VBOs
    arrayBuf.create();
    indexBuf.create();

    // Initializes cube geometry and transfers it to VBOs
    initCubeWireframeGeometry();
}

CubeWireframeGeometryEngine::~CubeWireframeGeometryEngine()
{
    arrayBuf.destroy();
    indexBuf.destroy();

#ifndef NDEBUG
    qDebug("[%s]", __PRETTY_FUNCTION__);
#endif
}

void CubeWireframeGeometryEngine::initCubeWireframeGeometry()
{
    // For cube wireframe we need only 8 vertices
    // (8 pieces of per-vertex data; no per-face differences)
    GLfloat cubeVertices[][3] = {
        // back face
        {-1.0f, -1.0f, -1.0f},
        { 1.0f, -1.0f, -1.0f},
        { 1.0f,  1.0f, -1.0f},
        {-1.0f,  1.0f, -1.0f},
        // front face
        {-1.0f, -1.0f,  1.0f},
        { 1.0f, -1.0f,  1.0f},
        { 1.0f,  1.0f,  1.0f},
        {-1.0f,  1.0f,  1.0f},
    };
    GLfloat cubeColors[][3] = {
        // back face
        {1.0f, 1.0f, 0.1f}, // special case for (-1,-1,-1)
        {1.0f, 0.1f, 0.1f}, // lighter red
        {1.0f, 0.1f, 0.1f},
        {1.0f, 0.1f, 0.1f},
        // front face
        {1.0f, 0.1f, 0.1f},
        {1.0f, 0.1f, 0.1f},
        {1.0f, 0.1f, 1.0f}, // special case for ( 1, 1, 1)
        {1.0f, 0.1f, 0.1f},
    };

    // Transfer vertex data to VBO 0: OpenGL vertex buffer for cube
    // - create and allocate OpenGL buffer to keep all cube data;
    //   there should be 8 vertex data points
    numVerticesData = sizeof(cubeVertices)/sizeof(cubeVertices[0]);
#ifndef NDEBUG
    qDebug("[%s] cube vertices = %2d (per-vertex data)", __PRETTY_FUNCTION__, numVerticesData);
#endif
    arrayBuf.bind();
    // - create and allocate OpenGL buffer to keep all cube index data
    arrayBuf.allocate(sizeof(cubeVertices) + sizeof(cubeColors));
    // - copy data from CPU to OpenGL buffer on GPU (positions + colors)
    int offset = 0;
    arrayBuf.write(offset, cubeVertices, sizeof(cubeVertices));
    offset += sizeof(cubeVertices);
    arrayBuf.write(offset, cubeColors,   sizeof(cubeColors));
    arrayBuf.release();

    // Indices for drawing cube wireframe using lines
    GLushort cubeIndices[][2] = {
        // back face
        {0, 1},
        {1, 2},
        {2, 3},
        {3, 0},
        // front face
        {4, 5},
        {5, 6},
        {6, 7},
        {7, 4},
        // connect
        {0, 4},
        {1, 5},
        {2, 6},
        {3, 7},
    };

    // Transfer index data to VBO 1: OpenGL index buffer for cube
    indexBuf.bind();
    // there should be (2 * (4+4+4 = 12) edges) = 24 indices
    // and there should be ((4+4+4 = 12) edges) = 12 lines (elements)
    numDrawElements = sizeof(cubeIndices)/sizeof(cubeIndices[0][0]);
#ifndef NDEBUG
    qDebug("[%s] cube indices  = %2d (2*%zd lines)", __PRETTY_FUNCTION__,
           numDrawElements, sizeof(cubeIndices)/sizeof(cubeIndices[0]));
#endif
    // - create and allocate OpenGL buffer to keep all cube index data
    //   and copy data from CPU to OpenGL buffer on GPU
    indexBuf.allocate(cubeIndices, sizeof(cubeIndices));
    indexBuf.release();
}

void CubeWireframeGeometryEngine::drawGeometry(QOpenGLShaderProgram *coloringShaderProgram)
{
    // Tell OpenGL which VBOs to use
    arrayBuf.bind();
    indexBuf.bind();

    int offset = 0;
    coloringShaderProgram->setAttributeBuffer("a_position", GL_FLOAT, offset, 3, 0);
    coloringShaderProgram->enableAttributeArray("a_position");
    offset += numVerticesData * 3 * sizeof(GLfloat);
    coloringShaderProgram->setAttributeBuffer("a_color", GL_FLOAT, offset, 3, 0);
    coloringShaderProgram->enableAttributeArray("a_color");

    arrayBuf.release();

    // Draw cube geometry using indices from VBO 1, and data from VBO 0
    glDrawElements(GL_LINES, numDrawElements, GL_UNSIGNED_SHORT, nullptr);

    coloringShaderProgram->disableAttributeArray("a_color");
    coloringShaderProgram->disableAttributeArray("a_position");

    indexBuf.release();
}
