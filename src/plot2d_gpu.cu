#include <cstdio>

#include "plot2d_common.hpp"
#include "plot2d_gpu.h"


__global__
void plot_func2d_kernel(unsigned char pixmap[], Grid2D grid)
{
    int ix = blockIdx.x * blockDim.x + threadIdx.x;
    int iy = blockIdx.y * blockDim.y + threadIdx.y;

    while (ix < grid.Nx()) {
        while (iy < grid.Ny()) {
            float x = grid.xi(ix, iy);
            float y = grid.yi(ix, iy);

            // 4 bytes (chars) per RGBA pixel
            plotPixel(&pixmap[4*grid.idx(ix, iy)],
                      func(x, y),
                      -1.0, 1.0);

            iy += blockDim.y * gridDim.y;
        }

        ix += blockDim.x * gridDim.x;
    }

}

__global__
void plot_func2d_kernel(float pixmap[], Grid2D grid)
{
    int ix = blockIdx.x * blockDim.x + threadIdx.x;
    int iy = blockIdx.y * blockDim.y + threadIdx.y;

    while (ix < grid.Nx()) {
        while (iy < grid.Ny()) {
            float x = grid.xi(ix, iy);
            float y = grid.yi(ix, iy);

            pixmap[grid.idx(ix, iy)] =
                rescaledValue(func(x, y), -1.0, 1.0);

            iy += blockDim.y * gridDim.y;
        }

        ix += blockDim.x * gridDim.x;
    }
}

void plot_func2d_gpu(unsigned char pixmap[], Grid2D grid)
{
    const int BLOCK_DIM = 16;
    dim3 blocksPerGrid((grid.Nx() + BLOCK_DIM - 1) / BLOCK_DIM,
                       (grid.Ny() + BLOCK_DIM - 1) / BLOCK_DIM);
    dim3 threadsPerBlock(BLOCK_DIM,
                         BLOCK_DIM);

    plot_func2d_kernel<<<blocksPerGrid, threadsPerBlock>>>(pixmap, grid);

    cudaError_t err = cudaGetLastError();

    static bool error_found = false;
    if (err != cudaSuccess && !error_found) {
        fprintf(stderr, "[%s] Failed to launch plot_func2d_kernel kernel (error code [%d]%s:%s)!\n",
                "plot_func2d_gpu(unsigned char pixmap[],...)",
                err, cudaGetErrorName(err), cudaGetErrorString(err));
        error_found = true;
    }
}

void plot_func2d_gpu(float pixmap[], Grid2D grid)
{
    const int BLOCK_DIM = 16;
    dim3 blocksPerGrid((grid.Nx() + BLOCK_DIM - 1) / BLOCK_DIM,
                       (grid.Ny() + BLOCK_DIM - 1) / BLOCK_DIM);
    dim3 threadsPerBlock(BLOCK_DIM,
                         BLOCK_DIM);

    plot_func2d_kernel<<<blocksPerGrid, threadsPerBlock>>>(pixmap, grid);

    cudaError_t err = cudaGetLastError();

    static bool error_found = false;
    if (err != cudaSuccess && !error_found) {
        fprintf(stderr, "[%s] Failed to launch plot_func2d_kernel kernel (error code [%d]%s:%s)!\n",
                "plot_func2d_gpu(float pixmap[],...)",
                err, cudaGetErrorName(err), cudaGetErrorString(err));
        error_found = true;
    }
}
