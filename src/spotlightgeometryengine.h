#ifndef SPOTLIGHTGEOMETRYENGINE_H
#define SPOTLIGHTGEOMETRYENGINE_H

#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>

class SpotlightGeometryEngine : protected QOpenGLFunctions
{
public:
    SpotlightGeometryEngine();
    virtual ~SpotlightGeometryEngine();

    void drawSpotlightGeometry(QOpenGLShaderProgram *coloringShaderProgram);

private:
    void initSpotlightGeometry();

    QOpenGLBuffer arrayBuf{QOpenGLBuffer::VertexBuffer};
    int numSpotlightVertices;
};

#endif // SPOTLIGHTGEOMETRYENGINE_H
