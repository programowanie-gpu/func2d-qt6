#ifndef FUNC2DBASE_H
#define FUNC2DBASE_H

#include <QOpenGLFunctions>
#include <QOpenGLBuffer>

#include "grid2d.h"


class Func2DBase : protected QOpenGLFunctions
{
public:
    Func2DBase(QOpenGLBuffer& pixmapBuf,
               float x0, float y0, float x1, float y1,
               int Nx, int Ny);
    virtual ~Func2DBase() = default;

    // getters
    Grid2D const & grid() const { return m_grid; };
    QOpenGLBuffer & pixmapBuf() const { return m_pixmapBuf; };

    /// compute f(x,y) values on a rectangular grid
    virtual void evaluate() = 0;

protected:
    Grid2D m_grid;
    QOpenGLBuffer &m_pixmapBuf;
};


#endif // FUNC2DBASE_H
