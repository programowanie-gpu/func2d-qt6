#ifndef PLOT2D_CPU_H
#define PLOT2D_CPU_H

#include "grid2d.h"


void plot_func2d_cpu(unsigned char pixmap[], Grid2D grid);
void plot_func2d_cpu(float pixmap[], Grid2D grid);

#endif // PLOT2D_CPU_H
