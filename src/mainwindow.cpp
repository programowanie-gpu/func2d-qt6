#include <QtWidgets>
#ifndef NDEBUG
#include <QDebug>
#endif

#include "mainwidget.h"

#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    // create OpenGL widget, set its size
    mainWidget = new MainWidget(this);

    // add OpenGL widget to main window
    setCentralWidget(mainWidget);
    // create and add controls
    createDockWindows();

    // create status bar
    statusBar()->showMessage("Ready");

    connect(mainWidget, &MainWidget::sendMessage,
            this, &MainWindow::showMessageInStatusbar);
}

MainWindow::~MainWindow() = default;


void MainWindow::keyPressEvent(QKeyEvent *event)
{
#ifndef NDEBUG
    qDebug("key pressed: [%d] '%s'",
           event->key(), event->text().toStdString().c_str());
#endif

    switch (event->key()) {
        case Qt::Key_Escape:
#ifndef NDEBUG
            qDebug("closing MainWindow");
#endif
            close();
            break;
        case Qt::Key_C:
            mainWidget->resetCameraView();
            break;
        case Qt::Key_R:
            mainWidget->resetRotation();
            break;
        default:
            QWidget::keyPressEvent(event);
    }
}


void MainWindow::createDockWindows()
{
#ifndef NDEBUG
    qDebug("[MainWindow::createDockWindows()]");
#endif

    // create dock for all the control widgets
    auto dock = new QDockWidget("Controls", this);
    dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    // remove close button from dock
    dock->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);

    // create group box with all the control widgets
    // to be used as the contents of the "Controls" dock
    auto *controlsGroupBox = new QGroupBox(this);
    controlsGroupBox->setFlat(true);

    // create control widgets
    // - create checkboxes (and in the future their layout)
#if 0
    // NOTE: currently there is no point light to represent and animate
    auto *animateLightCheckBox = new QCheckBox("animate light");
    animateLightCheckBox->setChecked(mainWidget->animate);
    animateLightCheckBox->setDisabled(true);
#endif


    // - separator
    auto *line = new QFrame();
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);

    // configure layout of widgets in "Controls" dock/group
    auto *controlsLayout = new QVBoxLayout;
    controlsGroupBox->setLayout(controlsLayout);
    // add individual widgets and widget groups
#if 0
    controlsLayout->addWidget(animateLightCheckBox);
#endif
    controlsLayout->addWidget(createDrawingGroup());
    controlsLayout->addWidget(createViewingGroup());
    controlsLayout->addWidget(createModelRotationGroup());
    // make widgets be aligned to the top
    controlsLayout->addStretch(1.0);

    // add control widgets to dock, attach dock
    dock->setWidget(controlsGroupBox);
    addDockWidget(Qt::RightDockWidgetArea, dock);

    // connect control widgets
#if 0
    connect(animateLightCheckBox, &QCheckBox::stateChanged,
            this, [=](int state) { this->mainWidget->animate = state; });
#endif
    connect(mainWidget, &MainWidget::plot2dInitialized,
            this, &MainWindow::updatePlot2dCheckboxes);
}

QGroupBox *MainWindow::createDrawingGroup()
{
#ifndef NDEBUG
    qDebug("[QGroupBox *MainWindow::createDrawingGroup()]");
#endif
    auto *drawingGroupBox = new QGroupBox("drawing");
    drawingGroupBox->setFlat(false);

    // - separator
    auto *line = new QFrame();
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);

    // - control additional geometries
    auto *boundingBoxCheckBox = new QCheckBox("bounding box");
    boundingBoxCheckBox->setChecked(mainWidget->showBoundingBox);

    // - control plot2d
    drawSurfaceCheckBox = new QCheckBox("draw surface");
    drawLinesCheckBox = new QCheckBox("draw lines (grid)");
    drawPointsCheckBox = new QCheckBox("draw point cloud");

    // layout
    QVBoxLayout *vbox = new QVBoxLayout;
    vbox->addWidget(boundingBoxCheckBox);
    vbox->addWidget(line);
    vbox->addWidget(drawSurfaceCheckBox);
    vbox->addWidget(drawLinesCheckBox);
    vbox->addWidget(drawPointsCheckBox);
    vbox->addStretch(1);
    drawingGroupBox->setLayout(vbox);

    // connect signals and slots
    // - controls for additional geometries
    connect(boundingBoxCheckBox, &QCheckBox::stateChanged,
            this, [=](int state) { this->mainWidget->showBoundingBox = state; });
    // - controls for mainWidget's plot2d
    connect(drawSurfaceCheckBox, &QCheckBox::stateChanged,
            mainWidget, &MainWidget::checkDrawSurface);
    connect(drawLinesCheckBox, &QCheckBox::stateChanged,
            mainWidget, &MainWidget::checkDrawLines);
    connect(drawPointsCheckBox, &QCheckBox::stateChanged,
            mainWidget, &MainWidget::checkDrawPoints);

    return drawingGroupBox;
}

QGroupBox *MainWindow::createViewingGroup()
{
#ifndef NDEBUG
    qDebug("[QGroupBox *MainWindow::createViewingGroup()]");
#endif
    auto *viewingGroupBox = new QGroupBox("view");
    viewingGroupBox->setFlat(false);
    //viewingGroupBox->setCheckable(true);
    //viewingGroupBox->setChecked(true);

    // widgets / controls
    auto *perspectiveCheckBox = new QCheckBox("perspective view");
    perspectiveCheckBox->setChecked(mainWidget->getPerspective());

    auto *alphaAngleSpinBox = new QDoubleSpinBox();
    alphaAngleSpinBox->setRange(0.0, 360.0);
    alphaAngleSpinBox->setValue(mainWidget->getCameraAlpha());
    alphaAngleSpinBox->setWrapping(true);
    alphaAngleSpinBox->setSuffix("°");
    alphaAngleSpinBox->setDecimals(1);
    alphaAngleSpinBox->setAlignment(Qt::AlignRight);

    auto *betaAngleSpinBox = new QDoubleSpinBox();
    betaAngleSpinBox->setRange(-90.0, 90.0);
    betaAngleSpinBox->setValue(mainWidget->getCameraBeta());
    betaAngleSpinBox->setWrapping(false);
    betaAngleSpinBox->setSuffix("°");
    betaAngleSpinBox->setDecimals(1);
    betaAngleSpinBox->setAlignment(Qt::AlignRight);

    auto *cameraDistanceSpinBox = new QDoubleSpinBox();
    cameraDistanceSpinBox->setValue(mainWidget->getCameraDistance());
    cameraDistanceSpinBox->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);
    cameraDistanceSpinBox->setWrapping(false);
    cameraDistanceSpinBox->setDecimals(2);
    //cameraDistanceSpinBox->setStatusTip("use mouse wheel to change distance between camera and plot");

    auto *resetCameraButton = new QPushButton("Reset &camera");
    auto *resetModelRotation = new QPushButton("Reset &rotation");

    // sub-layout
    QFormLayout *formLayout = new QFormLayout;
    formLayout->addRow("alpha", alphaAngleSpinBox);
    formLayout->addRow("beta", betaAngleSpinBox);
    formLayout->addRow("camera distance", cameraDistanceSpinBox);

    // layout
    QVBoxLayout *vbox = new QVBoxLayout;
    vbox->addWidget(perspectiveCheckBox);
    vbox->addLayout(formLayout);
    vbox->addWidget(resetCameraButton);
    vbox->addWidget(resetModelRotation);
    vbox->addStretch(1);
    viewingGroupBox->setLayout(vbox);

    // connect signals and slots
    connect(perspectiveCheckBox, &QCheckBox::stateChanged,
            this, [=](int state) {
        this->mainWidget->setPerspective(state);
        cameraDistanceSpinBox->setEnabled(state);
        // NOTE: camera rotation might affect also orthogonal projection
        alphaAngleSpinBox->setEnabled(state);
        betaAngleSpinBox->setEnabled(state);
    });
    connect(alphaAngleSpinBox, &QDoubleSpinBox::valueChanged,
            this, [=](double value) { this->mainWidget->setCameraAlpha(value); });
    connect(betaAngleSpinBox, &QDoubleSpinBox::valueChanged,
            this, [=](double value) { this->mainWidget->setCameraBeta(value); });
    connect(mainWidget, &MainWidget::cameraViewAnglesChanged,
            this, [=](float alpha, float beta) {
        alphaAngleSpinBox->setValue(alpha);
        betaAngleSpinBox->setValue(beta);
    });
    connect(cameraDistanceSpinBox, &QDoubleSpinBox::valueChanged,
            this, [=](double value) { this->mainWidget->setCameraDistance(value); });
    connect(mainWidget, &MainWidget::cameraDistanceValueChanged,
            cameraDistanceSpinBox, &QDoubleSpinBox::setValue);
    connect(resetCameraButton, &QPushButton::pressed,
            this, [=]() { this->mainWidget->resetCameraView(); });
    connect(resetModelRotation, &QPushButton::pressed,
            this, [=]() { this->mainWidget->resetRotation(0); });

    return viewingGroupBox;
}

QGroupBox *MainWindow::createModelRotationGroup()
{
#ifndef NDEBUG
    qDebug("[QGroupBox *MainWindow::createModelRotationGroup()]");
#endif
    auto *rotationGroupBox = new QGroupBox("rotation");
    rotationGroupBox->setFlat(false);

    // labels for values
    auto *pitchValueLabel = new QLabel(QString("%1°").arg(0.0));
    pitchValueLabel->setAlignment(Qt::AlignRight);
    pitchValueLabel->setMargin(2);
    auto *yawValueLabel   = new QLabel(QString("%1°").arg(0.0));
    yawValueLabel->setAlignment(Qt::AlignRight);
    yawValueLabel->setMargin(2);
    auto *rollValueLabel  = new QLabel(QString("%1°").arg(0.0));
    rollValueLabel->setAlignment(Qt::AlignRight);
    rollValueLabel->setMargin(2);
    auto *angularSpeedValue = new QLabel(QString("%1").arg(0.0));
    angularSpeedValue->setAlignment(Qt::AlignRight);
    angularSpeedValue->setMargin(2);

    // - separator
    auto *line = new QFrame();
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);

    // layout
    auto *formLayout = new QFormLayout;
    formLayout->addRow("pitch", pitchValueLabel);
    formLayout->addRow("yaw", yawValueLabel);
    formLayout->addRow("roll", rollValueLabel);
    formLayout->addRow(line);
    formLayout->addRow("angular speed", angularSpeedValue);
    // set layout
    rotationGroupBox->setLayout(formLayout);

    // connect signals and slots
    connect(mainWidget, &MainWidget::modelRotationChanged,
            this, [=](const QQuaternion &rotationQ) {
        float pitch, yaw, roll;
        rotationQ.getEulerAngles(&pitch, &yaw, &roll);

        pitchValueLabel->setText(QString("%1°").arg(pitch, 8, 'f', 2, u' '));
        yawValueLabel->setText(QString("%1°").arg(yaw, 8, 'f', 2, u' '));
        rollValueLabel->setText(QString("%1°").arg(roll, 8, 'f', 2, u' '));
    });
    connect(mainWidget, &MainWidget::modelAngularSpeedChanged,
            this, [=](qreal angularSpeed) {
        angularSpeedValue->setText(QString("%1").arg(angularSpeed));
    });

    return rotationGroupBox;
}

// -----------------------------------------------------------------------------------
// slots

void MainWindow::showMessageInStatusbar(const QString &message)
{
    statusBar()->showMessage(message);
#ifndef NDEBUG
    qDebug().nospace() << "[" << __PRETTY_FUNCTION__ << "] message = " << message;
#endif
}

void MainWindow::updatePlot2dCheckboxes(const SurfacePlotGL *plot2d)
{
    drawSurfaceCheckBox->setChecked(plot2d->checkDrawSurface);
    drawLinesCheckBox->setChecked(plot2d->checkDrawLines);
    drawPointsCheckBox->setChecked(plot2d->checkDrawPoints);
#ifndef NDEBUG
    qDebug("[%s] adjusted draw*CheckBox checkboxes based on plot2d->checkDraw*", __PRETTY_FUNCTION__);
#endif
}
