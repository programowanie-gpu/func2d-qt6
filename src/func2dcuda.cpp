// this header file contains only preprocessor macros and defines
#include "common.h" // IWYU pragma: keep

#include "func2dcuda.h"
#include "plot2d_gpu.h"


Func2DCUDA::Func2DCUDA(QOpenGLBuffer& pixmapBuf,
                       float x0, float y0, float x1, float y1,
                       int Nx, int Ny)
        : Func2DBase(pixmapBuf, x0, y0, x1, y1, Nx, Ny)
{
    create_pixmap_resource(__PRETTY_FUNCTION__);

#ifndef NDEBUG
    qDebug("[%s] registered graphics resource %p for OpenGL buffer object (VBO) %u",
           __PRETTY_FUNCTION__, m_pixmap_resource, m_pixmapBuf.bufferId());
#endif
}

Func2DCUDA::~Func2DCUDA()
{
    // remove graphics resource
    cudaError_t err =
        cudaGraphicsUnregisterResource(m_pixmap_resource);

#ifndef NDEBUG
    if (err) {
        qDebug("[%s] - CUDA error %s [%d]: %s",
               __PRETTY_FUNCTION__, cudaGetErrorName(err), err, cudaGetErrorString(err));
    } else {
        qDebug("[%s] unregistered graphics resource", __PRETTY_FUNCTION__);
    }
#endif
}

void Func2DCUDA::evaluate()
{
#ifndef NDEBUG
    static bool first_run = true;
    if (first_run) {
        qDebug("[%s] plot x=[%.2f:%.2f], y=[%.2f:%.2f] on [res=%p/VBO=%d]", __PRETTY_FUNCTION__,
               m_grid.x_min(), m_grid.x_max(), m_grid.y_min(), m_grid.y_max(),
               m_pixmap_resource, m_pixmapBuf.bufferId());
    }
#endif

    float *pixmap = nullptr;
    size_t pixmap_size = 0;

    if (!m_pixmap_resource)
        create_pixmap_resource(__PRETTY_FUNCTION__);

    cudaError_t err1, err2;
    err1 = cudaGraphicsMapResources(1, &m_pixmap_resource, nullptr);
    err2 = cudaGraphicsResourceGetMappedPointer((void**)&pixmap, &pixmap_size, m_pixmap_resource);
#ifndef NDEBUG
    if (err1 || err2) {
        qDebug("[%s] - error mapping resource, or getting mapped pointer", __PRETTY_FUNCTION__);
    }
    if (first_run) {
        qDebug("[%s] - mapped resource %p, got pointer %p, data size %lu = %zd*%zd*%zd (%lu)", __PRETTY_FUNCTION__,
               m_pixmap_resource, pixmap, (unsigned long)pixmap_size,
               m_grid.Nx(), m_grid.Ny(), sizeof(*pixmap), m_grid.n_points() * sizeof(*pixmap));
    }
#endif

    plot_func2d_gpu(pixmap, m_grid);

    err1 = cudaGraphicsUnmapResources(1, &m_pixmap_resource, nullptr);
#ifndef NDEBUG
    if (err1) {
        qDebug("[%s] - error unmapping resource", __PRETTY_FUNCTION__);
        qDebug("[%s]   %s [%d] = %s", __PRETTY_FUNCTION__,
               cudaGetErrorName(err1), err1, cudaGetErrorString(err1));
    }
#endif

#ifndef NDEBUG
    first_run = false;
#endif
}

void Func2DCUDA::create_pixmap_resource(const char* function_name)
{
    // create graphics resource
    cudaError_t err =
        cudaGraphicsGLRegisterBuffer(&m_pixmap_resource, m_pixmapBuf.bufferId(),
                                     cudaGraphicsMapFlagsWriteDiscard);

#ifndef NDEBUG
    if (err) {
        qDebug("![%s] CUDA error %s [%d]: %s",
               function_name, cudaGetErrorName(err), err, cudaGetErrorString(err));
    }
#endif
}
