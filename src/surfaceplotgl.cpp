#include <vector>

#include <QColor>

#include "grid2d.h"
#include "openglutils.h"
#include "surfaceplotgl.h"


void SurfacePlotGL::initializeGL()
{
#ifndef NDEBUG
    qDebug("[%s] start", __PRETTY_FUNCTION__);
#endif

    initializeOpenGLFunctions();

    // Surface::MeshTriangles use clockwise (GL_CW) orientation
    glFrontFace(GL_CW); // TODO: save state in SurfacePlotGL::draw(), maybe make it per mesh

    // create shader program for $z = f(x,y)$ plot
    m_plotShaderProgram.addShaderFromSourceFile(QOpenGLShader::Vertex,
                                                ":/glsl/plot2d.vert");
    m_plotShaderProgram.addShaderFromSourceFile(QOpenGLShader::Fragment,
                                                ":/glsl/plot2d.frag");
#ifndef NDEBUG
    if (!m_plotShaderProgram.link()) {
        qDebug() << "SurfacePlotGL: could not compile plot shader program:\n"
                 << m_plotShaderProgram.log();
    }
    qDebug("  built plot2d shader program for SurfacePlotGL [programId=%d]", m_plotShaderProgram.programId());
#endif // !defined(NDEBUG)

    // create data for per-vertex parameters
    // - (x,y) do not depend on function, do not change
    std::vector<Point2D> verticesXY;
    m_plottedFunction->grid().fill_xy(verticesXY);
    int sizeXY = m_plottedFunction->grid().n_points()*2*sizeof(GLfloat);
#ifndef NDEBUG
    qDebug("  filled verticesXY, size = %d bytes (%zd x %zd = %zd elements)",
           sizeXY, m_plottedFunction->grid().Nx(), m_plottedFunction->grid().Ny(),
           m_plottedFunction->grid().n_points());
#endif

    m_vertexXY.create();
    m_vertexXY.bind();
    // allocate buffer and copy data to OpenGL buffer; data is no longer needed
    m_vertexXY.allocate(verticesXY.data(), sizeXY);
    m_vertexXY.release();
#ifndef NDEBUG
    qDebug("  created and filled vertexXY VBO [%d] (defined range: [%g, %g] x [%g, %g])",
           m_vertexXY.bufferId(),
           m_plottedFunction->grid().x_min(), m_plottedFunction->grid().x_max(),
           m_plottedFunction->grid().y_min(), m_plottedFunction->grid().y_max());
    qDebug("  - 1st  point at (%g, %g)", verticesXY[0].x, verticesXY[0].y);
    qDebug("  - 2nd  point at (%g, %g)", verticesXY[1].x, verticesXY[1].y);
    qDebug("  - last point at (%g, %g) index: %zd",
           verticesXY[verticesXY.size()-1].x, verticesXY[verticesXY.size()-1].y,
           verticesXY.size());
#endif // !defined(NDEBUG)

    m_surfaceMesh->initializeGL();
    m_surfaceGrid->initializeGL();

    // NOTE: we might want to evaluate / compute function values here

#ifndef NDEBUG
    qDebug("[%s] end", __PRETTY_FUNCTION__);
#endif
}

void SurfacePlotGL::draw(QMatrix4x4 mvpMatrix)
{
#ifndef NDEBUG
    static bool first_run = true;

    // select shader program
    if (first_run) {
        qDebug("[%s] shader program id: %d", __PRETTY_FUNCTION__,
               m_plotShaderProgram.programId());
    }
#endif
    m_plotShaderProgram.bind();

    // set values of uniform parameters
    // - transformation, and extra transformation
#ifndef NDEBUG
    if (first_run) {
        qDebug() << "  mvpMatrix:\n" << mvpMatrix;
    }
#endif
    m_plotShaderProgram.setUniformValue("mvpMatrix", mvpMatrix);

    // set values of per-vertex parameters
    // - positions[, normals, colors, texturing]
#ifndef NDEBUG
    if (first_run) {
        qDebug("  vertexXY buffer id: %d (%s)",
               m_vertexXY.bufferId(), describeQOpenGLBufferType(m_vertexXY.type()));
    }
#endif
    m_vertexXY.bind();
    m_plotShaderProgram.setAttributeBuffer("vertexXY", GL_FLOAT, 0, 2);
    m_plotShaderProgram.enableAttributeArray("vertexXY");
    m_vertexXY.release();

#ifndef NDEBUG
    if (first_run) {
        qDebug("  vertexZ  buffer id: %d (%s); function ptr = %p",
               m_plottedFunction->pixmapBuf().bufferId(),
               describeQOpenGLBufferType(m_plottedFunction->pixmapBuf().type()),
               m_plottedFunction);
    }
#endif
    m_plottedFunction->pixmapBuf().bind();
    m_plotShaderProgram.setAttributeBuffer("vertexZ", GL_FLOAT, 0, 1);
    m_plotShaderProgram.enableAttributeArray("vertexZ");
    m_plottedFunction->pixmapBuf().release();


    // draw
    if (polygonOffset) {
        /*
         * void glPolygonOffset( GLfloat factor,
         *                       GLfloat units );
         *
         * factor:
         *      Specifies a scale factor that is used to create
         *      a variable depth offset for each polygon. The initial value is 0.
         *
         * units:
         *      Is multiplied by an implementation-specific value
         *      to create a constant depth offset. The initial value is 0.
         *
         * The value of the offset is factor×DZ+r×units, where DZ is a measurement of the change
         * in depth relative to the screen area of the polygon, and r is the smallest value
         * that is guaranteed to produce a resolvable offset for a given implementation.
         * The offset is added before the depth test is performed and before the value
         * is written into the depth buffer.
         *
         * ------------------------------------------------------------------------------------
         * From http://www.glprogramming.com/red/chapter06.html#name4
         * via https://stackoverflow.com/q/13431174/46058
         *
         * To achieve a nice rendering of the highlighted solid object without visual artifacts,
         * you can either add a positive offset to the solid object (push it away from you)
         * or a negative offset to the wireframe (pull it towards you).
         *
         * Small, non-zero values for factor, such as 0.75 or 1.0, are probably enough
         * to generate distinct depth values and eliminate the unpleasant visual artifacts.
         */
        glPolygonOffset(1, 0);
        glEnable(GL_POLYGON_OFFSET_FILL);
#ifndef NDEBUG
        if (first_run) {
            qDebug("  setting polygon %s offset to (%d, %d)", "fill", 1, 0);
        }
#endif
    }
    // - surface
    if (checkDrawSurface) {
#ifndef NDEBUG
        if (first_run) {
            qDebug("  drawing surface plot using %d indices",
                   m_surfaceMesh->count());
        }
#endif
        // draw surface a little darker
        m_plotShaderProgram.setUniformValue("ambientColor", QColor(180, 180, 180));
        m_surfaceMesh->drawElements();
    }
    // - grid / lines
    if (checkDrawLines) {
#ifndef NDEBUG
        if (first_run) {
            qDebug("  drawing %zd x %zd grid using %d indices",
                   m_plottedFunction->grid().Nx(),
                   m_plottedFunction->grid().Ny(),
                   m_surfaceGrid->count());
        }
#endif
        // draw lines with default brightness
        m_plotShaderProgram.setUniformValue("ambientColor", QColor(255, 255, 255));
        m_surfaceGrid->drawElements();
    }
    // - point cloud
    if (checkDrawPoints) {
#ifndef NDEBUG
        if (first_run) {
            qDebug("  drawing point cloud of %zd x %zd = %zd points",
                   m_plottedFunction->grid().Nx(),
                   m_plottedFunction->grid().Ny(),
                   m_plottedFunction->grid().n_points());
        }
#endif
        // draw points with default brightness
        m_plotShaderProgram.setUniformValue("ambientColor", QColor(255, 255, 255));
        glDrawArrays(GL_POINTS, 0,
                     static_cast<GLsizei>(m_plottedFunction->grid().n_points()));
    }

    // release
    m_plotShaderProgram.disableAttributeArray("vertexXY");
    m_plotShaderProgram.disableAttributeArray("vertexZ");

    m_plotShaderProgram.release();

#ifndef NDEBUG
    if (first_run) {
        qDebug("[%s] end", __PRETTY_FUNCTION__);
    }
    first_run = false;
#endif
}

void SurfacePlotGL::cleanup()
{
#ifndef NDEBUG
    qDebug("[%s] start (vertexXY bufferId=%d)",
           __PRETTY_FUNCTION__, m_vertexXY.bufferId());
#endif

    if (!m_vertexXY.isCreated() || m_vertexXY.bufferId() == 0) {
#ifndef NDEBUG
        // MAYBE promote to qWarning()
        qDebug("[%s] end (EARLY)", __PRETTY_FUNCTION__);
#endif
        return;
    }
    m_vertexXY.destroy();
#ifndef NDEBUG
    qDebug("  vertexXY after destruction [bufferId=%d]", m_vertexXY.bufferId());

    qDebug("[%s] end", __PRETTY_FUNCTION__);
#endif
}
