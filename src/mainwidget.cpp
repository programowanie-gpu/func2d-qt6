#include <QColor>
#include <QTimer>
#include <QString>
#ifndef NDEBUG
#include <QList>
#include <QSurfaceFormat>
#include <QOpenGLContext>
#include <QOpenGLDebugLogger>
#endif // !defined(NDEBUG)

// this header file contains only preprocessor macros and defines
#include "common.h" // IWYU pragma: keep
#include "mainwindow.h"
#include "mainwidget.h"


MainWidget::MainWidget(QWidget *parent) :
    QOpenGLWidget(parent)
{
#ifndef NDEBUG
    prepareOpenGLDebugLogging();
#endif

    // Use QBasicTimer because it's faster than QTimer
    timer.start(12, this);
}

MainWidget::~MainWidget()
{
#ifndef NDEBUG
    qDebug("[%s] start", __PRETTY_FUNCTION__);
#endif

    // Make sure the context is current when deleting
    // OpenGL resources, like the texture and the buffers.
    makeCurrent();
    pixelBuf.destroy();

    delete plot2d;
    delete func2d;

    //delete spotlightGeometries;
    delete cubeWireframeGeometries;
    doneCurrent();

#ifndef NDEBUG
    qDebug("[%s] end", __PRETTY_FUNCTION__);
#endif
}

QSize MainWidget::minimumSizeHint() const
{
    return {50, 50};
}

QSize MainWidget::sizeHint() const
{
    return {512, 512};
}

void MainWidget::initializeGL()
{
#ifndef NDEBUG
    qDebug("[%s] start", __PRETTY_FUNCTION__);
#endif
    initializeOpenGLFunctions();

#ifndef NDEBUG
    enableOpenGLDebugLogging(false);
#endif

    glClearColor(0, 0, 0, 1);

    initShaders();
    initBuffers();

    //spotlightGeometries = new SpotlightGeometryEngine;
    cubeWireframeGeometries = new CubeWireframeGeometryEngine;

#ifdef HAVE_CUDA
    func2d = new Func2DCUDA(pixelBuf, -1.0f, -1.0f, 1.0f, 1.0f, 75, 75);
#else
    func2d = new Func2DGL(pixelBuf, -1.0f, -1.0f, 1.0f, 1.0f, 75, 75);
#endif
#ifndef NDEBUG
    qDebug("[%s] created Func2D..., grid %zd x %zd, using buffer %d", __PRETTY_FUNCTION__,
           func2d->grid().Nx(), func2d->grid().Ny(), pixelBuf.bufferId());
#endif // !defined(NDEBUG)
    plot2d = new SurfacePlotGL(func2d);
#ifndef NDEBUG
    qDebug("[%s] created and initialized SurfacePlotGL", __PRETTY_FUNCTION__);
#endif // !defined(NDEBUG)
    emit plot2dInitialized(plot2d);

    QString openGLinfo = QString("OpenGL version %1 (%2:%3)")
            .arg(QString::fromLatin1(glGetString(GL_VERSION)),
                 QString::fromLatin1(glGetString(GL_VENDOR)),
                 QString::fromLatin1(glGetString(GL_RENDERER)));
    emit sendMessage(openGLinfo);

#ifndef NDEBUG
    qDebug("[%s] end", __PRETTY_FUNCTION__);
#endif // !defined(NDEBUG)
}

void MainWidget::resizeGL(int width, int height)
{
#ifndef NDEBUG
    qDebug("[%s] start (%d, %d)", __PRETTY_FUNCTION__, width, height);
#endif

    width  = std::max(width,  1); // to avoid division by zero
    height = std::max(height, 1); // to avoid division by zero

    setProjectionMatrix(width, height);

    glViewport(0, 0, width, height);
#ifndef NDEBUG
    qDebug("[%s] end", __PRETTY_FUNCTION__);
#endif
}

void MainWidget::setPerspective(bool _perspective)
{
#ifndef NDEBUG
    qDebug("[%s] perspective %d -> %d", __PRETTY_FUNCTION__,
           this->perspective, _perspective);
#endif
    this->perspective = _perspective;

    setProjectionMatrix(width(), height());
}

void MainWidget::resetCameraView()
{
    // TODO: introduce constant for initial camera view params
    alpha =  25.0f;
    beta  = -25.0f;
    cameraDistance = 2.5f;

    emit cameraDistanceValueChanged(cameraDistance);
    emit cameraViewAnglesChanged(alpha, beta);
    update();
#ifndef NDEBUG
    qDebug("camera view reset to alpha=%f deg, beta=%f deg, cameraDistance=%f",
           alpha, beta, cameraDistance);
#endif
}

void MainWidget::resetRotation(int to_state)
{
    static int state = 0; ///< zero-based rotation reset state number
    const int max_states = 2;

    // handle optional parameter
    if (0 <= to_state && to_state < max_states)
        state = to_state;

    // helper variable, outside switch
    QMatrix4x4 mMatrix; // model matrix

    switch (state % max_states) {
        case 1:
            // Tait-Bryan angles (nautical angles, or Cardan angles)
            // z-y'-x'' - yaw, pitch, roll  or  heading, elevation, bank
            // rotation angle in degrees (0..360 or -180..180)
            mMatrix.rotate(-80.0 /* xAngle */, 1, 0, 0);
            mMatrix.rotate(  -2.0 /* yAngle */, 0, 1, 0);
            mMatrix.rotate(  0.0 /* zAngle */, 0, 0, 1);

#ifndef NDEBUG
            qDebug("rotating with Cardan angles: x_rot=% .2f, y_rot=% .2f, z_rot=% .2f (degrees)",
                   -80.0, -2.0, 0.0);
#endif
            rotationQ = QQuaternion::fromRotationMatrix(mMatrix.toGenericMatrix<3,3>());
            break;
        case 0:
        default:
            rotationQ = QQuaternion();
            break;
    }
    angularSpeed = 0.0;
    emit modelRotationChanged(rotationQ);
    emit modelAngularSpeedChanged(angularSpeed);
    update();

#ifndef NDEBUG
    qDebug("rotation reset to state %d of %d", state+1, max_states);
#endif
    // if no optional parameter
    if (to_state < 0 || max_states <= to_state)
        state = (state+1) % max_states;
}

void MainWidget::setProjectionMatrix(const float width, const float height)
{
    pMatrix.setToIdentity();
    if (perspective) {
        pMatrix.perspective(vfovAngleDegrees,
                            width / height,
                            0.001, 1000);
    } else {
        // rescale scene to match widget aspect ratio
        float hratio = 1.0f, vratio = 1.0f;
        if (width > height) {
            hratio = width / height;
        } else {
            vratio = height / width;
        }
        // clipping plane must be slightly larger than sqrt(3) = 1.73205...
        // to avoid clipping when bounding cube is [-1, 1]^3
        pMatrix.ortho(-1.0f*hratio, +1.0f*hratio,
                      -1.0f*vratio, +1.0f*vratio,
                      -1.75f, 1.75f);
    }
}

void MainWidget::paintGL()
{
    //qDebug("[%s] start", __PRETTY_FUNCTION__);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // model transformation
    QMatrix4x4 mMatrix; ///< model transformation matrix (the scene)
    mMatrix.setToIdentity();
    //mMatrix.translate(0.0, 0.0, -5.0);
    mMatrix.rotate(rotationQ);

    // view transformation (camera)
    QMatrix4x4 vMatrix; ///< view transformation matrix (whole world / camera)
    if (perspective) {
        QMatrix4x4 cameraTransformation;
        cameraTransformation.rotate(alpha, 0, 1, 0); // positive values: looks from the right
        cameraTransformation.rotate(beta,  1, 0, 0); // negative values: looks from the top
        // up direction defines rotation around $z$ axis

        QVector3D cameraPosition    = cameraTransformation.map(QVector3D(0, 0, cameraDistance));
        QVector3D cameraUpDirection = cameraTransformation.map(QVector3D(0, 1, 0));

        vMatrix.lookAt(cameraPosition,     // eye position
                       QVector3D(0, 0, 0), // center, position to look at
                       cameraUpDirection); // orientation
    }

    // normal matrix, for lighting model
    QMatrix4x4 mvMatrix = vMatrix * mMatrix;

#if 0
    // light position
    QMatrix4x4 lightTransformation;
    lightTransformation.rotate(lightAngleDegrees, 0, 1, 0);

    QVector3D lightPosition = lightTransformation.map(QVector3D(0, 1, 1));
#endif

    // compute values
    func2d->evaluate();
    // draw the surface plot
    plot2d->draw(pMatrix * mvMatrix);

    if (showBoundingBox) {
        // draw the wireframe of the [-1,1]×[-1,1]×[-1,1] bounding cube
        coloringShaderProgram.bind();
        coloringShaderProgram.setUniformValue("mvp_matrix", pMatrix * vMatrix * mMatrix);

        // set line width
        // NOTE: wide lines have been deprecated (line width > 1.0), but there is no simple replacement
        //glPushAttrib(GL_LINE_BIT); // not supported anymore
        float old_linewidth = 1.0f;
        glGetFloatv(GL_LINE_WIDTH, &old_linewidth);
        glLineWidth(boundingBoxLineWidth);

        cubeWireframeGeometries->drawGeometry(&coloringShaderProgram);

        // reset/restore line width
        //glPopAttrib(); // not supported anymore
        glLineWidth(old_linewidth);

        coloringShaderProgram.release();
    }

#if 0
    // draw the spotlight representation:
    mMatrix.setToIdentity();
    // - put the spotlight model where spotlight is
    mMatrix.translate(lightPosition);
    // - rotate the model so it points in the direction of the light...
    mMatrix.rotate(lightAngleDegrees, 0, 1, 0);
    //   ... taking into account its initial direction
    mMatrix.rotate(45, 1, 0, 0);
    // - rescale themodel
    mMatrix.scale(0.1);

    // To improve performance, we can enable face culling.
    // NOTE: face culling must be turned off for 2d surfaces.
    glEnable(GL_CULL_FACE);

    coloringShaderProgram.bind();

    coloringShaderProgram.setUniformValue("mvp_matrix", pMatrix * vMatrix * mMatrix);

    // - draw spotlight representation geometry
    spotlightGeometries->drawSpotlightGeometry(&coloringShaderProgram);

    coloringShaderProgram.release();

    // For flat 2d surfaces, where we want to be able to see both sides
    // should have face culling disabled (it can be handled in shaders).
    glDisable(GL_CULL_FACE);
#endif

    //qDebug("[%s] end", __PRETTY_FUNCTION__);
}


/* --------------------------------------------------------------------- */
// QWidget's event handlers

void MainWidget::mousePressEvent(QMouseEvent *event)
{
    lastMousePosition = mousePressPosition = event->position();

    event->accept();
}

void MainWidget::mouseMoveEvent(QMouseEvent *event)
{
    QPointF currentMousePosition = event->position();
    qreal deltaX = currentMousePosition.x() - lastMousePosition.x();
    qreal deltaY = currentMousePosition.y() - lastMousePosition.y();

    if (perspective && (event->buttons() & Qt::LeftButton)) { // rotate camera
        // wrap alpha to 0..360
        alpha -= deltaX;
        while (alpha < 0) {
            alpha += 360;
        }
        while (alpha >= 360) {
            alpha -= 360;
        }
        // clamp beta to -90..90
        beta -= deltaY;
        if (beta < -90) {
            beta = -90;
        }
        if (beta >  90) {
            beta =  90;
        }
        // update state
        emit cameraViewAnglesChanged(alpha, beta);
        update();

    } else if (event->buttons() & Qt::RightButton) { // rotate cube (model)
        auto diff = QVector2D(currentMousePosition - mousePressPosition);

        // Rotation axis is perpendicular to the mouse position difference vector
        auto n = QVector3D(diff.y(), diff.x(), 0.0).normalized();

        // Accelerate angular speed relative to the length of the mouse sweep
        qreal acc = diff.length() / 100.0;

        // Calculate new rotation axis as weighted sum
        rotationAxis = (rotationAxis * angularSpeed + n * acc).normalized();

        // Increase angular speed
        angularSpeed += acc;
        emit modelAngularSpeedChanged(angularSpeed);
    }

    lastMousePosition = event->position();
    event->accept();
}

void MainWidget::wheelEvent(QWheelEvent *event)
{
    if (!perspective) {
#ifndef NDEBUG
        qDebug("[%s] no perspective", __PRETTY_FUNCTION__);
#endif
        event->accept();

        return;
    }

#ifndef NDEBUG
    qDebug("[%s] cameraDistance = %g", __PRETTY_FUNCTION__, cameraDistance);
#endif

    int delta = event->angleDelta().y();
    if (delta != 0) { // vertical wheel
        if (delta < 0) {
            cameraDistance *= 1.1;
        } else {
            cameraDistance *= 0.9;
        }

        emit cameraDistanceValueChanged(cameraDistance);
        update();
    }
    event->accept();
}

void MainWidget::timerEvent(QTimerEvent *)
{
#if 0
    if (animate) {

        // animate spotlight position
        lightAngleDegrees += 1;
        while (lightAngleDegrees >= 360) {
            lightAngleDegrees -= 360;
        }

    }
#endif

    // model rotation with inertia
    // - nothing to do if speed is 0 (rotation stopped)
    if (angularSpeed == 0)
        return;
    // - decrease angular speed (friction)
    angularSpeed *= 0.99;

    // - stop rotation when speed goes below threshold...
    if (angularSpeed < 0.01) {
        angularSpeed = 0.0;
    } else {
        //   ... or update rotation
        rotationQ = QQuaternion::fromAxisAndAngle(rotationAxis, angularSpeed) * rotationQ;
        emit modelRotationChanged(rotationQ);
        update();
    }
    emit modelAngularSpeedChanged(angularSpeed);

    // Request an update
    update();
}

void MainWidget::initShaderProgram(QOpenGLShaderProgram &shaderProgram, const char *basename)
{
#ifndef NDEBUG
    qDebug("[%s] start, basename = %s", __PRETTY_FUNCTION__, basename);
#endif

    QString vertShaderName = QString(":/glsl/%1.vert").arg(basename);
    QString fragShaderName = QString(":/glsl/%1.frag").arg(basename);

    // Compile vertex shader
    if (!shaderProgram.addShaderFromSourceFile(QOpenGLShader::Vertex,
                                               vertShaderName))
        close();

    // Compile fragment shader
    if (!shaderProgram.addShaderFromSourceFile(QOpenGLShader::Fragment,
                                               fragShaderName))
        close();

    // Link shader pipeline
    if (!shaderProgram.link())
        close();

#ifndef NDEBUG
    qDebug("[%s] end [%d]", __PRETTY_FUNCTION__,
           shaderProgram.programId());
#endif
}

void MainWidget::initShaders()
{
#ifndef NDEBUG
    qDebug("[%s] start", __PRETTY_FUNCTION__);
#endif

    // If we want to render 3D images, we need to enable depth testing.
    glEnable(GL_DEPTH_TEST);
    // smooth lines for wireframe
    glEnable(GL_LINE_SMOOTH);

    initShaderProgram(coloringShaderProgram, "colors");

#ifndef NDEBUG
    qDebug("[%s] end", __PRETTY_FUNCTION__);
#endif
}

void MainWidget::initBuffers()
{
#ifndef NDEBUG
    qDebug("[%s] start", __PRETTY_FUNCTION__);
#endif

    pixelBuf.create();

#ifndef NDEBUG
    qDebug("[%s] end: pixelBuf VBO = %d", __PRETTY_FUNCTION__,
           pixelBuf.bufferId());
#endif
}

#ifndef NDEBUG
void MainWidget::prepareOpenGLDebugLogging()
{
    QSurfaceFormat format;
    format.setProfile(QSurfaceFormat::CompatibilityProfile);
    format.setOption(QSurfaceFormat::DebugContext);
    setFormat(format);
}

void MainWidget::enableOpenGLDebugLogging(bool synchronousLogging) const
{
    QOpenGLContext *ctx = QOpenGLContext::currentContext();
    qDebug("[%s] OpenGL %d.%d", __PRETTY_FUNCTION__,
           ctx->format().majorVersion(), ctx->format().minorVersion());
    auto *logger = new QOpenGLDebugLogger((QObject *) this);
    if (!logger->initialize()) // initializes in the current context, i.e. ctx
        return;

    const QList<QOpenGLDebugMessage> messages = logger->loggedMessages();
    for (const QOpenGLDebugMessage &message : messages)
        qDebug() << message;

    connect(logger, &QOpenGLDebugLogger::messageLogged, this, [&](const QOpenGLDebugMessage & debugMessage) {
        qDebug() << debugMessage;
    });
    //logger->enableMessages();
    logger->disableMessages(QOpenGLDebugMessage::AnySource, QOpenGLDebugMessage::AnyType,
                            QOpenGLDebugMessage::NotificationSeverity);
    // deprecated behavior warning can happen on each draw, e.g. about glLineWidth()
    logger->disableMessages(QOpenGLDebugMessage::AnySource, QOpenGLDebugMessage::DeprecatedBehaviorType,
                            QOpenGLDebugMessage::AnySeverity);
    if (synchronousLogging)
        logger->startLogging(QOpenGLDebugLogger::SynchronousLogging);
    else
        logger->startLogging();

    QOpenGLDebugMessage message =
        QOpenGLDebugMessage::createApplicationMessage(QStringLiteral("Started OpenGL logging"));
    logger->logMessage(message);
}
#endif // !defined(NDEBUG)

/* --------------------------------------------------------------------- */
// public slots

void MainWidget::checkDrawSurface(int status)
{
    if (plot2d) {
        plot2d->checkDrawSurface = status;
        update();
    }
}

void MainWidget::checkDrawLines(int status)
{
    if (plot2d) {
        plot2d->checkDrawLines = status;
        update();
    }
}

void MainWidget::checkDrawPoints(int status)
{
    if (plot2d) {
        plot2d->checkDrawPoints = status;
        update();
    }
}
