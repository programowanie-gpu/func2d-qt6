#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QOpenGLBuffer>

#include <QMatrix4x4>
#include <QVector>
#include <QVector3D>
#include <QQuaternion>

#include <QWheelEvent>
#include <QBasicTimer>
#include <QTimerEvent>

//#include "spotlightgeometryengine.h"
#include "cubewireframegeometryengine.h"
#include "texturedrectgeometryengine.h"
#ifdef HAVE_CUDA
#include "func2dcuda.h"
#else
#include "func2dgl.h"
#endif
#include "surfaceplotgl.h"


class MainWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    using QOpenGLWidget::QOpenGLWidget;
    explicit MainWidget(QWidget *parent = nullptr);
    ~MainWidget();

    // QWidget interface
    QSize sizeHint() const override;
    QSize minimumSizeHint() const override;

    // public controls
    // - getters and setters (some may be replaced by making fields public, or class friends)
    void setPerspective(bool perspective);
    bool getPerspective() const { return perspective; };
    void setCameraDistance(float distance) { cameraDistance = distance; update(); };
    float getCameraDistance() const { return cameraDistance; };
    void setCameraAlpha(float angle_degrees) { alpha = angle_degrees; update(); };
    void setCameraBeta(float angle_degrees)  { beta  = angle_degrees; update(); };
    float getCameraAlpha() const { return alpha; };
    float getCameraBeta() const  { return beta; };
    // - multi-state toggles
    void resetCameraView();
    void resetRotation(int to_state = -1);
    // - fields (maybe to be made private)
    bool animate = true; // NOTE: currently unused
    bool showBoundingBox = true;
    float boundingBoxLineWidth = 2.0f;

protected:
    // QOpenGLWidget interface
    void initializeGL() override;
    void resizeGL(int width, int height) override;
    void paintGL() override;

    // QWidget interface
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;
    void timerEvent(QTimerEvent *e) override;

    void initShaders();
    void initBuffers();

private:
    QOpenGLBuffer pixelBuf{QOpenGLBuffer::VertexBuffer};
#ifdef HAVE_CUDA
    Func2DCUDA *func2d = nullptr;
#else
    Func2DGL *func2d = nullptr;
#endif
    SurfacePlotGL *plot2d = nullptr;

    QOpenGLShaderProgram coloringShaderProgram;

#if 0
    SpotlightGeometryEngine *spotlightGeometries = nullptr;
    double lightAngleDegrees = 0.0f; ///< rotation angle of spotlight position around $y$ axis
#endif

    CubeWireframeGeometryEngine *cubeWireframeGeometries = nullptr;

    // perspective projection
    QMatrix4x4 pMatrix; ///< perspective projection matrix
    float vfovAngleDegrees = 60.0f; ///< camera vertical field of view for perspective projection
    bool perspective = true; ///< use perspective projection

    // TODO: better names for camera rotation angles `alpha` and `beta`
    float alpha =  25.0f; ///< rotation angle around world's $y$ axis, moves to the side
    float beta  = -25.0f; ///< rotation angle around world's $x$ axis, tilts the camera
    float cameraDistance = 2.5f; ///< camera distance from the origin, along $z$ axis

    // model rotation (with inertia)
    QVector3D rotationAxis;
    qreal angularSpeed = 0.0;
    QQuaternion rotationQ;

    // mouse event support
    QPointF lastMousePosition;
    QPointF mousePressPosition;

    // animation
    QBasicTimer timer;

private:
    void initShaderProgram(QOpenGLShaderProgram &shaderProgram, const char *basename);
    void setProjectionMatrix(const float width, const float height);

#ifndef NDEBUG
    void prepareOpenGLDebugLogging();
    void enableOpenGLDebugLogging(bool synchronousLogging = false) const;
#endif

signals:
#pragma clang diagnostic push
#pragma ide diagnostic ignored "NotImplementedFunctions"
    void sendMessage(const QString &message);
    void plot2dInitialized(const SurfacePlotGL *plot2d);
    void cameraDistanceValueChanged(double value);
    void cameraViewAnglesChanged(float alpha, float beta);
    void modelRotationChanged(const QQuaternion &rotationQ);
    void modelAngularSpeedChanged(qreal value);
#pragma clang diagnostic pop

public slots:
    // slots for checkboxes, updating plot2d
    void checkDrawSurface(int status);
    void checkDrawLines(int status);
    void checkDrawPoints(int status);
};

#endif // MAINWIDGET_H
