#ifndef OPENGLUTILS_H
#define OPENGLUTILS_H

#include <QOpenGLBuffer>


const char* describeQOpenGLBufferType(QOpenGLBuffer::Type type);

#endif //OPENGLUTILS_H
