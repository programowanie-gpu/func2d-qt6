#ifndef FUNC2DGL_H
#define FUNC2DGL_H

#include <QOpenGLFunctions>
#include <QOpenGLBuffer>

#include "grid2d.h"
#include "func2dbase.h"


class Func2DGL : public Func2DBase
{
public:
    Func2DGL(QOpenGLBuffer& pixmapBuf,
             float x0, float y0, float x1, float y1,
             int Nx, int Ny);
    virtual ~Func2DGL();

    virtual void evaluate() override;

protected:
    float *m_pixmap = nullptr;
};

#endif // FUNC2DGL_H
