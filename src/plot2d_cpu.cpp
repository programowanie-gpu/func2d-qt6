#include "plot2d_common.hpp"
#include "plot2d_cpu.h"


void plot_func2d_cpu(unsigned char pixmap[], Grid2D grid)
{
    int ix, iy;

    for (ix = 0; ix < grid.Nx(); ix++) {
        for (iy = 0; iy < grid.Ny(); iy++) {
            float x = grid.xi(ix, iy);
            float y = grid.yi(ix, iy);

            // 4 bytes (chars) per RGBA pixel
            plotPixel(&pixmap[4*grid.idx(ix, iy)],
                      func(x, y),
                      -1.0, 1.0);
        }
    }

}

void plot_func2d_cpu(float pixmap[], Grid2D grid)
{
    int ix, iy;

    for (ix = 0; ix < grid.Nx(); ix++) {
        for (iy = 0; iy < grid.Ny(); iy++) {
            float x = grid.xi(ix, iy);
            float y = grid.yi(ix, iy);

            pixmap[grid.idx(ix, iy)] =
                rescaledValue(func(x, y), -1.0, 1.0);
        }
    }
}
