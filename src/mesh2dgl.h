#ifndef MESH2DGL_H
#define MESH2DGL_H

#include <vector>

#include <QOpenGLBuffer>
#include <QOpenGLFunctions>

#include "grid2d.h"


class Mesh2DGL : protected QOpenGLFunctions
{
public:
    Mesh2DGL() { m_indexBuffer.create(); m_indexBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw); }

#ifndef NDEBUG
    virtual ~Mesh2DGL() { qDebug("%s", __PRETTY_FUNCTION__); destroy(); }
#else
    virtual ~Mesh2DGL() = default;
#endif

    // void glDrawElements(GLenum mode, GLsizei count, GLenum type, const GLvoid *indices);
    // - mode: Specifies what kind of primitives to render.
    // - count: Specifies the number of elements to be rendered.
    // - type: Specifies the type of the values in `indices`.
    // - indices: Specifies an offset of the first index in the array
    //   in the data store of the buffer currently bound to
    //   the GL_ELEMENT_ARRAY_BUFFER target.
    virtual GLenum draw_mode() const = 0;
    virtual GLsizei count() const { return static_cast<GLsizei>(m_indexData.size()); }
    virtual GLenum index_type() const { return GL_UNSIGNED_INT; }

    // those are never overridden
    void initializeGL();
    void drawElements();
    // this is called from virtual destructor
    void destroy() { m_indexBuffer.destroy(); }

protected:
    QOpenGLBuffer m_indexBuffer{QOpenGLBuffer::IndexBuffer};
    std::vector<GLuint> m_indexData;
};

namespace Surface {
    // this class is here only for inheritance chain, to distinguish types of meshes
    class Mesh : public Mesh2DGL {
    public:
        Mesh() : Mesh2DGL() {}
#ifndef NDEBUG
        virtual ~Mesh() { qDebug("%s", __PRETTY_FUNCTION__); }
#else
        virtual ~Mesh() = default;
#endif

        virtual GLenum index_type() const;
    };

    class MeshTriangles : public Mesh
    {
    public:
        MeshTriangles(GLuint Nx, GLuint Ny);
        explicit MeshTriangles(const Grid2D &grid)
                : MeshTriangles(static_cast<GLuint>(grid.Nx()), static_cast<GLuint>(grid.Ny())) {}

        virtual GLenum draw_mode() const override;
    };

    // this class is here only for inheritance chain, to distinguish types of grids
    class Grid : public Mesh2DGL {
    public:
        Grid() : Mesh2DGL() {}

#ifndef NDEBUG
        virtual ~Grid() { qDebug("%s", __PRETTY_FUNCTION__); }
#else
        virtual ~Grid() = default;
#endif

        virtual GLenum index_type() const;
    };

    class GridLines : public Grid
    {
    public:
        GridLines(GLuint Nx, GLuint Ny);
        explicit GridLines(const Grid2D &grid)
                : GridLines(static_cast<GLuint>(grid.Nx()), static_cast<GLuint>(grid.Ny())) {}

        virtual GLenum draw_mode() const override;
    };
}

#endif // MESH2DGL_H
