// this header file contains only preprocessor macros and defines
#include "common.h" // IWYU pragma: keep

#include "grid2d.h"
#include "func2dbase.h"

Func2DBase::Func2DBase(QOpenGLBuffer& pixmapBuf,
                       float x0, float y0, float x1, float y1,
                       int Nx, int Ny)
    : m_pixmapBuf(pixmapBuf), m_grid(x0, x1, y0, y1, Nx, Ny)
{
    // sanity check
    if (Nx < 2 || Ny < 2) {
        if (Nx < 2) Nx = 2;
        if (Ny < 2) Ny = 2;

        m_grid = Grid2D(x0, x1, y0, y1, Nx, Ny);
    }

    // allocate OpenGL buffer, creating it if necessary
    // TODO: pixmap element type, and width, should be template parameters
    if (!m_pixmapBuf.isCreated())
        m_pixmapBuf.create();

    m_pixmapBuf.bind();
    m_pixmapBuf.allocate(Nx * Ny * sizeof(float));
    m_pixmapBuf.release();

#ifndef NDEBUG
    qDebug("[%s] allocated %d x %d floats = %zd bytes OpenGL buffer object (VBO) %u",
           __PRETTY_FUNCTION__, Nx, Ny, Nx*Ny*sizeof(float), m_pixmapBuf.bufferId());
#endif
}
