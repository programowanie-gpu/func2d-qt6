#ifndef PLOT2D_COMMON_HPP
#define PLOT2D_COMMON_HPP

// To include M_PI macro without Qt includes, via <cmath>
// see https://stackoverflow.com/questions/6563810/m-pi-works-with-math-h-but-not-with-cmath-in-vis
// see https://learn.microsoft.com/en-us/cpp/c-runtime-library/math-constants
#define _USE_MATH_DEFINES // for C++
#include <cmath>

// in case something included <cmath> earlier, or precompiled headers are used
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#include "common.h"


typedef enum rgba_color_idx {
    cR = 0,
    cG,
    cB,
    cA
} rgba_color_idx_t;

__host__ __device__
static
float func(float x, float y)
{
    // TODO: replace hypotf() with std::hypot(), etc.
    // testing cutoffs
#if 0
    if (fabs(x) + fabs(y) < 0.2f)
        return  3.0f;
    else if (fabs(hypotf(x, y) - 1.0f) < 0.02f)
        return -3.0f;
#endif

    //return sinf(x*2.0f*M_PI) + cosf(y*2.0f*M_PI);
    //return sinf(x*M_PI) + (2.0f*(y - floorf(y)) - 1.0f);
    //return (2.0f*(x - floorf(x)) - 1.0) + (2.0f*(y - floorf(y)) - 1.0f);
    //return 2*expf(-(x*x/4 + y*y)/3.0f)*sinf(4*sqrtf(x*x/4+y*y)*M_PI);
    //return 4.0f*expf(-(x*x/0.25f+y*y/0.25f)) - 2.0f;
    float d = hypotf(x, y - 0.3f) * 4.0f;
    return (1.0f - d*d) * expf((d * d + -8.0f*x*y - x) / -2.0f);
}

__host__ __device__
static
void plotPixel(unsigned char pixel[], float val, float zmin, float zmax)
{
    // sanity check?
    if (zmax < zmin) {
        float tmp = zmin;
        zmin = zmax;
        zmax = tmp;
    }
    if (fabs(zmax - zmin) < 1e-9)
        zmax = zmin + 1.0f;

    // out of range values
    if (val < zmin) {
        // alternative solution would be to clamp values: val = zmin;
        // blue (cool) for values below the minimum
        pixel[cR] = 0; pixel[cG] = 0; pixel[cB] = 255; pixel[cA] = 255;
        return;
    } else if (val > zmax) {
        // alternative solution would be to clamp values: val = zmax;
        // red (hot) for values above the maximum
        pixel[cR] = 255; pixel[cG] = 0; pixel[cB] = 0; pixel[cA] = 255;
        return;
    }

    // rescale values to 0.0..1.0 range
    val = (val - zmin)/(zmax - zmin);

    // use grayscale colormap
    pixel[cR] = pixel[cG] = pixel[cB] = (char)floor(255.0*val);
    pixel[cA] = 255;
}

__host__ __device__
static
float rescaledValue(float val, float zmin, float zmax)
{
    // sanity check?
    if (zmax < zmin) {
        float tmp = zmin;
        zmin = zmax;
        zmax = tmp;
    }
    if (fabs(zmax - zmin) < 1e-9)
        zmax = zmin + 1.0f;

    // rescale values to -1.0..1.0 range
    // without any overflow handling
    if (zmin == -1.0f && zmax == +1.0f)
        return val;
    else
        return 2.0f*((val - zmin)/(zmax - zmin)) - 1.0f;
}

#endif // PLOT2D_COMMON_HPP
