#ifndef COMMON_H
#define COMMON_H

#ifdef _MSC_VER
// __PRETTY_FUNCTION__ is a GNU extension
// __FUNCSIG__ is its MS Visual Studio equivalent
#ifndef __PRETTY_FUNCTION__
#define __PRETTY_FUNCTION__ __FUNCSIG__
#endif
#endif

#ifndef __CUDACC__ // Defined when compiling CUDA source files.
// define some CUDA C++ keywords, so they can be used in plain C++ code
#ifndef __host__
#define __host__
#define __device__
#endif // !defined(__host__)
#endif // !defined(__CUDACC__)

#endif // COMMON_H
