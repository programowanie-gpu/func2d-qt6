// this 'common.h' header file contains only preprocessor macros and defines
#include "common.h" // IWYU pragma: keep
#include "texturedrectgeometryengine.h"

TexturedRectGeometryEngine::TexturedRectGeometryEngine()
    : numVerticesData(4), // 4 vertices in a rectangle
      numDrawElements(4)  // 4 indices for triangle strip of a rectangle
{
#ifndef NDEBUG
    qDebug("[%s]", __PRETTY_FUNCTION__);
#endif
    initializeOpenGLFunctions();

    // Generate 2 VBOs
    arrayBuf.create();
    indexBuf.create();

    // Initializes cube geometry and transfers it to VBOs
    initGeometry();
}

TexturedRectGeometryEngine::~TexturedRectGeometryEngine()
{
    arrayBuf.destroy();
    indexBuf.destroy();

#ifndef NDEBUG
    qDebug("[%s]", __PRETTY_FUNCTION__);
#endif
}

void TexturedRectGeometryEngine::initGeometry()
{
    // for square we need only 4 vertices
    // TODO: make it parameterizable, keeping the axis-align restriction
    /*
     *  v3     v2        y ^
     *   +-----+           |
     *   |     |      -----+---->
     *   |     |           |   x
     *   +-----+           |
     *  v0     v1
     */
    GLfloat rectVertices[][3] = {
#if 1 // square in $xy$ plane, $z = 0$
        {-1.0f, -1.0f,  0.0f}, // v0
        { 1.0f, -1.0f,  0.0f}, // v1
        { 1.0f,  1.0f,  0.0f}, // v2
        {-1.0f,  1.0f,  0.0f}, // v3
#endif
#if 0 // square in $xy$ plane, $z = -1.03$
        {-1.0f, -1.0f,  -1.03f}, // v0
        { 1.0f, -1.0f,  -1.03f}, // v1
        { 1.0f,  1.0f,  -1.03f}, // v2
        {-1.0f,  1.0f,  -1.03f}, // v3
#endif
#if 0 // square in $xz$ plane, $y = -1.02$
        {-1.0f, -1.02f,  1.0f}, // v0
        { 1.0f, -1.02f,  1.0f}, // v1
        { 1.0f, -1.02f, -1.0f}, // v2
        {-1.0f, -1.02f, -1.0f}, // v3
#endif
    };
    /*
     * (0,1)  (1,1)
     *  v3-----v2
     *   |     |
     *   |     |
     *  v0-----v1
     * (0,0)  (1,0)
     */
    GLfloat rectTextureCoordinates[][2] = {
        {0, 0}, // v0
        {1, 0}, // v1
        {1, 1}, // v2
        {0, 1}, // v3
    };
    // Transfer vertex data to VBO 0: OpenGL vertex buffer for cube
    // - create and allocate OpenGL buffer to keep all cube data;
    //   there should be 8 vertex data points
    numVerticesData = sizeof(rectVertices)/sizeof(rectVertices[0]);
#ifndef NDEBUG
    qDebug("[%s] rectangle vertices = %d (per-vertex data)", __PRETTY_FUNCTION__, numVerticesData);
#endif
    arrayBuf.bind();
    // - create and allocate OpenGL buffer to keep all rectangle index data
    arrayBuf.allocate(sizeof(rectVertices) + sizeof(rectTextureCoordinates));
    // - copy data from CPU to OpenGL buffer on GPU (positions + texture coordinates)
    int offset = 0;
    arrayBuf.write(offset, rectVertices, sizeof(rectVertices));
    offset += sizeof(rectVertices);
    arrayBuf.write(offset, rectTextureCoordinates, sizeof(rectTextureCoordinates));
    arrayBuf.release();

    // Indices for drawing rectangle using triangle strip
    // Note that in this case drawing could be done with non-indexed glDrawArrays()
    /*
     *  v3-->--v2
     *   | \   |
     *   |  ^  |
     *   |   \ |
     *  v0-->--v1
     */
    GLushort rectIndices[][2] = {
        0,  1,  3,  2, // triangle strip ( v0,  v1,  v3,  v2)
    };
    // Transfer index data to VBO 1: OpenGL index buffer for rectangle
    indexBuf.bind();
    numDrawElements = sizeof(rectIndices)/sizeof(rectIndices[0][0]);
#ifndef NDEBUG
    qDebug("[%s] rectangle indices  = %d (triangle strip)",
           __PRETTY_FUNCTION__, numDrawElements);
#endif
    // - create and allocate OpenGL buffer to keep all index data
    //   and copy data from CPU to OpenGL buffer on GPU
    indexBuf.allocate(rectIndices, sizeof(rectIndices));
    indexBuf.release();
}

void TexturedRectGeometryEngine::drawGeometry(QOpenGLShaderProgram *texturingShaderProgram)
{
    // Tell OpenGL which VBOs to use
    arrayBuf.bind();
    indexBuf.bind();

    int offset = 0;
    // a_position is vec4: x, y, z, w - but we pass only x, y, z
    texturingShaderProgram->setAttributeBuffer("a_position", GL_FLOAT, offset, 3, 0);
    texturingShaderProgram->enableAttributeArray("a_position");
    offset += numVerticesData * 3 * sizeof(GLfloat);
    // a_texture_coord is vec2: u, v
    texturingShaderProgram->setAttributeBuffer("a_texture_coord", GL_FLOAT, offset, 2, 0);
    texturingShaderProgram->enableAttributeArray("a_texture_coord");

    arrayBuf.release();

    // Draw rectangle geometry using indices from VBO 1, and data from VBO 0
    glDrawElements(GL_TRIANGLE_STRIP, numDrawElements, GL_UNSIGNED_SHORT, nullptr);

    texturingShaderProgram->disableAttributeArray("a_color");
    texturingShaderProgram->disableAttributeArray("a_texture_coord");

    indexBuf.release();
}
