#ifndef FUNC2DCUDA_H
#define FUNC2DCUDA_H

#include <QOpenGLFunctions>
#include <QOpenGLBuffer>

#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

#include "grid2d.h"
#include "func2dbase.h"


class Func2DCUDA : public Func2DBase
{
public:
    Func2DCUDA(QOpenGLBuffer& pixmapBuf,
               float x0, float y0, float x1, float y1,
               int Nx, int Ny);
    virtual ~Func2DCUDA();

    virtual void evaluate() override;

protected:
    cudaGraphicsResource *m_pixmap_resource = nullptr;

    void create_pixmap_resource(const char* function_name);
};

#endif // FUNC2DCUDA_H
