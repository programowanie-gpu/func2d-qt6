#include <QVector2D>
#include <QVector3D>

#include "common.h"
#include "spotlightgeometryengine.h"


SpotlightGeometryEngine::SpotlightGeometryEngine()
    : numSpotlightVertices((4+2)*3) // 4 triangular sides, 1 square base composed of 2 triangles
{
#ifndef NDEBUG
    qDebug("[%s]", __PRETTY_FUNCTION__);
#endif
    initializeOpenGLFunctions();

    // Generate 1 VBOs
    arrayBuf.create();

    // Initializes spotlight indicator (pyramid) geometry and transfers it to VBOs
    initSpotlightGeometry();
}

SpotlightGeometryEngine::~SpotlightGeometryEngine()
{
    arrayBuf.destroy();

#ifndef NDEBUG
    qDebug("[%s]", __PRETTY_FUNCTION__);
#endif
}

void SpotlightGeometryEngine::initSpotlightGeometry()
{
    QVector<QVector3D> spotlightVertices;
    QVector<QVector3D> spotlightColors;

    spotlightVertices
            << QVector3D(   0,    1,    0) << QVector3D(-0.5,    0,  0.5) << QVector3D( 0.5,    0,  0.5) // Front
            << QVector3D(   0,    1,    0) << QVector3D( 0.5,    0, -0.5) << QVector3D(-0.5,    0, -0.5) // Back
            << QVector3D(   0,    1,    0) << QVector3D(-0.5,    0, -0.5) << QVector3D(-0.5,    0,  0.5) // Left
            << QVector3D(   0,    1,    0) << QVector3D( 0.5,    0,  0.5) << QVector3D( 0.5,    0, -0.5) // Right
            << QVector3D(-0.5,    0, -0.5) << QVector3D( 0.5,    0, -0.5) << QVector3D( 0.5,    0,  0.5) // Bottom
            << QVector3D( 0.5,    0,  0.5) << QVector3D(-0.5,    0,  0.5) << QVector3D(-0.5,    0, -0.5);
    spotlightColors
            << QVector3D(0.2, 0.2, 0.2) << QVector3D(0.2, 0.2, 0.2) << QVector3D(0.2, 0.2, 0.2) // Front
            << QVector3D(0.2, 0.2, 0.2) << QVector3D(0.2, 0.2, 0.2) << QVector3D(0.2, 0.2, 0.2) // Back
            << QVector3D(0.2, 0.2, 0.2) << QVector3D(0.2, 0.2, 0.2) << QVector3D(0.2, 0.2, 0.2) // Left
            << QVector3D(0.2, 0.2, 0.2) << QVector3D(0.2, 0.2, 0.2) << QVector3D(0.2, 0.2, 0.2) // Right
            << QVector3D(  1,   1,   1) << QVector3D(  1,   1,   1) << QVector3D(  1,   1,   1) // Bottom
            << QVector3D(  1,   1,   1) << QVector3D(  1,   1,   1) << QVector3D(  1,   1,   1);

    // OpenGL vertex buffer for spotlight
    // - create and allocate OpenGL buffer to keep all cube data
    numSpotlightVertices = spotlightVertices.size();
#ifndef NDEBUG
    qDebug("[%s] spotlight vertices = %d", __PRETTY_FUNCTION__, numSpotlightVertices);
#endif
    arrayBuf.bind();
    arrayBuf.allocate(numSpotlightVertices * (3 + 3) * sizeof(GLfloat));

    // - copy data from CPU to OpenGL buffer on GPU
    int offset = 0;
    arrayBuf.write(offset, spotlightVertices.constData(), numSpotlightVertices * 3 * sizeof(GLfloat));
    offset += numSpotlightVertices * 3 * sizeof(GLfloat);
    arrayBuf.write(offset, spotlightColors.constData(), numSpotlightVertices * 3 * sizeof(GLfloat));

    arrayBuf.release();
}

void SpotlightGeometryEngine::drawSpotlightGeometry(QOpenGLShaderProgram *coloringShaderProgram)
{
    // Tell OpenGL which VBOs to use
    arrayBuf.bind();

    // Tell OpenGL programmable pipeline how to locate vertex position data
    int offset = 0;
    coloringShaderProgram->setAttributeBuffer("a_position", GL_FLOAT, offset, 3, 0);
    coloringShaderProgram->enableAttributeArray("a_position");
    // Tell OpenGL programmable pipeline how to locate vertex color data
    offset += numSpotlightVertices * 3 * sizeof(GLfloat);
    coloringShaderProgram->setAttributeBuffer("a_color", GL_FLOAT, offset, 3, 0);
    coloringShaderProgram->enableAttributeArray("a_color");

    arrayBuf.release();

    // Draw "spotlight" geometry
    glDrawArrays(GL_TRIANGLES, 0, numSpotlightVertices);

    // Cleanup
    coloringShaderProgram->disableAttributeArray("a_position");
    coloringShaderProgram->disableAttributeArray("a_color");
}
