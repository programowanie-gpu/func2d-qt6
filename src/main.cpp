#include "common.h"
#include "mainwindow.h"

#include <QApplication>
#ifdef QT_NO_OPENGL
#include <QLabel>
#else
#include <QSurfaceFormat>
#endif

#ifdef HAVE_CUDA
#include <cuda_runtime_api.h>
#endif

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

#ifndef QT_NO_OPENGL
    MainWindow window;

    // prepare for OpenGL debug logging
    QSurfaceFormat format;
    //format.setDepthBufferSize(24);
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setOption(QSurfaceFormat::DebugContext);

    QSurfaceFormat::setDefaultFormat(format);

#ifdef HAVE_CUDA
    window.setWindowTitle("Plot3D z=f(x,y) [OpenGL and CUDA with Qt6]");

    // Explicitly set device 0
    cudaSetDevice(0);
#ifndef NDEBUG
    qDebug("[%s] using CUDA GPU device %d", __PRETTY_FUNCTION__, 0);
#endif // !defined(NDEBUG)
#else
    window.setWindowTitle("Plot3D z=f(x,y) [OpenGL with Qt6]");
#endif
    window.show();
#else
    QLabel note("OpenGL Support required");
    note.show();
#endif

    return app.exec();
}
