# func2d-qt6 - Plot f(x,y) using CUDA, OpenGL and Qt6

Draws a surface plot of a function of two parameters
$z = f(x,y)$.  It uses CUDA or CPU code to compute function values,
OpenGL to draw the result, Qt6 for user interface, and CMake build system.

Currently, computation is always done on GPU if CMake detects working
CUDA Toolkit SDK (unless `USE_CUDA_IF_POSSIBLE` build configuration variable
is set to `false`), and on CPU otherwise.

![app screenshot, with perspective projection](screenshots/screenshot-plot3d-v2.func2d-qt6.png)

This screenshot shows surface plot of the following function:

$$ f(x,y) = (1 - d^2) \exp\left(\frac{d^2 - 8 x y - x}{2}\right) $$

where $d = 4 \sqrt{x^2 + (y - 0.3)^2}$.

## Differences with func2d-qt5

This is the list of differences between this project and its predecessor,
[func2d-qt5][], besides the obvious: change in targeted version of the
Qt library.

- func2d-qt6 uses CMake build system, instead of using qmake
  (and `*.pro` files)
- it does not split Func2D* classes into FuncXYEval without storage,
  and FuncXY which includes storage, as unnecessary abstraction
- Func2D* do not calculate minimum and maximum from computed values;
  there are no `z_min()` or `z_max()` methods
- it does not include `eulerrecover.{cpp,h}` (the main part of which
  could be replaced by use of relevant `QQuaternion` methods)
- func2d-qt6 puts source files in `src/`, and shaders in `glsl/`directory
- it lacks UI for controlling rotation angles, relying on mouse controls
  for setting model rotation (drag with right mouse button)
- there are no old-style `check_gl_errors()` function in `openglutils.cpp`

Some of those, but not all, might be added to func2d-qt6 later.

### TODO for func2d-qt5 features

Below there is a list of func2d-qt5 features that are planned to be
included in func2d-qt6 (this project).

- [ ] describe how to change camera distance (effectively zoom level),
  maybe also adding user interface for this
- [x] turn off printing of debugging information with `NDEBUG` macro
- [x] turn off real-time OpenGL logging with `NDEBUG` macro

[func2d-qt5]: https://gitlab.com/programowanie-gpu/func2d-qt5 "Programowanie GPU / func2d-qt5 · GitLab"
