#version 460

in vec4 varying_color;
out vec4 fragColor;

void main(void)
{
    fragColor = varying_color;
}
