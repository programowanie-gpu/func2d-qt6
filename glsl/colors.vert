#version 460

uniform mat4 mvp_matrix;

in vec4 a_position;
in vec4 a_color;

out vec4 varying_color;

void main()
{
    // Pass the color value to the fragment shader for interpolation
    varying_color = a_color;
    // Calculate vertex position in screen space
    gl_Position = mvp_matrix * a_position;
}
