#version 460

uniform mat4 mvpMatrix;

in vec2 vertexXY;
in float vertexZ; // to uncomment

out vec4 varyingColor;


// takes an float argument x (x should be: 0.0 <= x <= 1.0)
// returns a vec4 value which represents an RGBA color
vec4 colormap(float x);

void main()
{
    gl_Position = mvpMatrix * vec4(vertexXY, vertexZ, 1.0);
    varyingColor = colormap(0.5*vertexZ + 0.5); // -1..1 -> 0..1
}

// MATLAB_jet colormap (http://www.mathworks.com/products/matlab/)
// https://github.com/kbinani/colormap-shaders/blob/master/shaders/glsl/MATLAB_jet.frag
float colormap_red(float x) {
    if (x < 0.7) {
        return 4.0 * x - 1.5;
    } else {
        return -4.0 * x + 4.5;
    }
}

float colormap_green(float x) {
    if (x < 0.5) {
        return 4.0 * x - 0.5;
    } else {
        return -4.0 * x + 3.5;
    }
}

float colormap_blue(float x) {
    if (x < 0.3) {
        return 4.0 * x + 0.5;
    } else {
        return -4.0 * x + 2.5;
    }
}

vec4 colormap(float x) {
    float r = clamp(colormap_red(x), 0.0, 1.0);
    float g = clamp(colormap_green(x), 0.0, 1.0);
    float b = clamp(colormap_blue(x), 0.0, 1.0);
    return vec4(r, g, b, 1.0);
}
