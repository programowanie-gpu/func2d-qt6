#version 460

uniform sampler2D tex;

in vec2 varying_texture_coord;
out vec4 fragColor;


vec4 colormap(float x);

void main(void)
{
#if __VERSION__ > 120
    float value = texture(tex, varying_texture_coord).r;
#else
    float value = texture2D(tex, varying_texture_coord).r;
#endif

    fragColor = colormap(value);
}

vec4 colormap(float x)
{
    if (x > 1.0)
        return vec4(1.0, 0.0, 0.0, 1.0);
    if (x < 0.0)
        return vec4(0.0, 0.0, 1.0, 1.0);

    return vec4(x, x, x, 1.0);
}
