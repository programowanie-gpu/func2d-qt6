#version 460

uniform vec4 ambientColor;

in vec4 varyingColor;

// output color as RGBA of a fragment's pixel,
// to be used in place of gl_FragColor built-in variable
out vec4 fragColor;


void main()
{
    // make back-side darker
    float factor;
    if (gl_FrontFacing)
        factor = 1.0;
    else
        factor = 0.5;

    fragColor = factor * ambientColor * varyingColor;
}
