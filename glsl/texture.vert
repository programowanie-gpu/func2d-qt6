#version 460

uniform mat4 mvp_matrix;

in vec4 a_position;
in vec2 a_texture_coord;

out vec2 varying_texture_coord;

void main()
{
    // Pass the texture coordinates to the fragment shader for interpolation
    varying_texture_coord = a_texture_coord;
    // Calculate vertex position in screen space
    gl_Position = mvp_matrix * a_position;
}
